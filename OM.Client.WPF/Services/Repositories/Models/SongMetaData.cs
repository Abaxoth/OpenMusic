﻿namespace OM.Client.WPF.Services.Repositories.Models
{
    internal class SongMetaData
    {
        public long SongId { get; set; }
        public string SongFilePath { get; set; }
    }
}

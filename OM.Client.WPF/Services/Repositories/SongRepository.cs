﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using OM.Client.App.Http;
using OM.Client.WPF.Services.Abstractions;
using OM.Client.WPF.Services.Repositories.Models;
using OM.Client.WPF.Settings;
using OM.Service.Dto;

namespace OM.Client.WPF.Services.Repositories
{
    internal class SongRepository : ISongRepository
    {
        private readonly List<SongMetaData> _songs = new();

        public SongRepository(AppHttpClient httpClient)
        {
            HttpClient = httpClient;
        }

        public SongRepository(AppHttpClient httpClient, List<SongMetaData> songs)
        {
            HttpClient = httpClient;
            _songs = songs;
        }

        protected AppHttpClient HttpClient { get; }


        public async Task<string> ProvideSongFileAsync(SongPreviewDto song)
        {
            var localSong = _songs.FirstOrDefault(x => x.SongId == song.SongId);
            if (localSong != null) return localSong.SongFilePath;

            var songData = await HttpClient.DownloadSongAsync(song.SongId);
            AddToRepository(song, songData);
            return await ProvideSongFileAsync(song);
        }

        private void AddToRepository(SongPreviewDto song, byte[] songData)
        {
            var songFilePath = Path.Combine(AppSettings.SongsDirectory, $"Song_{song.SongId}.mp3");
            var metaFilePath = Path.Combine(AppSettings.SongMetaDataDirectory, $"Song_{song.SongId}.json");
            var metaData = new SongMetaData()
            {
                SongFilePath = songFilePath,
                SongId = song.SongId,
            };

            _songs.Add(metaData);
            File.WriteAllText(metaFilePath, JObject.FromObject(metaData).ToString());
            File.WriteAllBytes(songFilePath, songData);
        }
    }
}

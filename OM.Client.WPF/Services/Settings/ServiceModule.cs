﻿using System;
using System.Reflection;
using Ninject.Modules;
using OM.Client.App.Http;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Player;
using OM.Client.WPF.Services.Abstractions;
using OM.Client.WPF.Services.Providers;
using OM.Core.Extensions;

namespace OM.Client.WPF.Services.Settings
{
    internal class ServiceModule: NinjectModule
    {
        public override void Load()
        {
            BindViewModels();

            Bind<AppHttpClient>().ToSelf();
            Bind<IServiceProvider>().To<ServiceProvider>().InTransientScope();
            Bind<IViewInitializer>().To<ViewInitializer>().InTransientScope();
            Bind<IOpenMusicPlayer>().To<OpenMusicPlayer>().InSingletonScope();

            Bind<ISongRepository>().ToProvider<SongRepositoryProvider>().InSingletonScope();
        }

        private void BindViewModels()
        {
            var viewModels = Assembly.GetExecutingAssembly()
                .FindTypesWithAttribute<ViewModelAttribute>();
            foreach (var viewModel in viewModels)
                Bind(viewModel).ToSelf();
        }
    }
}

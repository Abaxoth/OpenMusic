﻿using Ninject;
using OM.Client.App;

namespace OM.Client.WPF.Services.Settings
{
    public static class ServiceConfiguration
    {
        static ServiceConfiguration()
        {
            Kernel = new StandardKernel(new ServiceModule(), new ClientAppServiceModule());
        }

        public static IKernel Kernel { get; }
    }
}

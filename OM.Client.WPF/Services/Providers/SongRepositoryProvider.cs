﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.POCO;
using Newtonsoft.Json;
using Ninject.Activation;
using OM.Client.App.Http;
using OM.Client.WPF.Services.Repositories;
using OM.Client.WPF.Services.Repositories.Models;
using OM.Client.WPF.Settings;

namespace OM.Client.WPF.Services.Providers
{
    internal class SongRepositoryProvider: Provider<SongRepository>
    {
        public SongRepositoryProvider(AppHttpClient appHttp)
        {
            HttpClient = appHttp;
        }

        protected AppHttpClient HttpClient { get; }

        protected override SongRepository CreateInstance(IContext context)
        {
            var meta = LoadSongMetaDataCollection();
            var repository = new SongRepository(HttpClient, meta);
            return repository;
        }

        private List<SongMetaData> LoadSongMetaDataCollection()
        {
            var files = Directory.GetFiles(AppSettings.SongMetaDataDirectory);
            var metaDataCollection = files
                .Select(x => JsonConvert.DeserializeObject<SongMetaData>(File.ReadAllText(x)));
            return metaDataCollection.ToList();
        }
    }
}

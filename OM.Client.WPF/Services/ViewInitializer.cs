﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Helpers;
using OM.Client.WPF.Services.Abstractions;
using OM.Client.WPF.ViewModels.Pages;
using OM.Core.Extensions;

namespace OM.Client.WPF.Services
{
    public class ViewInitializer: IViewInitializer
    {
        private readonly IServiceProvider _serviceProvider;

        public ViewInitializer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void ShowByViewModel<TViewModel>(TViewModel viewModel, bool asDialog)
            where TViewModel : BaseViewModel
        {
            var viewType = typeof(TViewModel).GetAttribute<ViewModelAttribute>().ViewType;
            var viewInstance = InitializeView(viewType);
            var window = (viewInstance as Window).NotNull($"View type: {viewType} is not window");
            window.DataContext = viewModel;

            if (asDialog) window.ShowDialog();
            else window.Show();
        }

        public void ShowByViewModel<TViewModel>(bool asDialog)
            where TViewModel : BaseViewModel
        {
            ShowByViewModel(_serviceProvider.GetService<TViewModel>(), asDialog);
        }

        public TView InitializeView<TView>(BaseViewModel viewModel)
            where TView : ContentControl, new()
        {
            var view = InitializeView<TView>();
            view.DataContext = viewModel;
            return view;
        }

        public object InitializeView(Type viewType)
        {
            var viewModels = Assembly.GetExecutingAssembly()
                .GetTypes().Where(x => x.GetCustomAttribute<ViewModelAttribute>() != null);

            foreach (var viewModelType in viewModels)
            {
                var attribute = viewModelType.GetAttribute<ViewModelAttribute>();
                if (attribute.ViewType != viewType) continue;

                var context = _serviceProvider.GetService(viewModelType);
                context.NotNull($"Can't find service: {viewModelType}");

                var view = (ContentControl)Activator.CreateInstance(viewType);
                view = view.NotNull($"Can't create instance of type: {viewType}");
                view.DataContext = context;
                ConfigureEvents(view);
                return view;
            }

            throw new Exception($"Can't initialize view of type: {viewType}");
        }

        public TView InitializeView<TView>()
            where TView : ContentControl, new()
        {
            return (TView)InitializeView(typeof(TView));
        }

        private void ConfigureEvents(ContentControl control)
        {
            if (control is Window window)
                ViewEventsConfiguration.RegisterEvents(window);

        }
    }
}

﻿using System;
using System.Windows.Controls;
using OM.Client.WPF.ViewModels.Pages;

namespace OM.Client.WPF.Services.Abstractions
{
    public interface IViewInitializer
    {
        void ShowByViewModel<TViewModel>(TViewModel viewModel, bool asDialog)
            where TViewModel : BaseViewModel;

        void ShowByViewModel<TViewModel>(bool asDialog)
            where TViewModel : BaseViewModel;

        TView InitializeView<TView>(BaseViewModel viewModel)
            where TView : ContentControl, new();


        object InitializeView(Type viewType);

        TView InitializeView<TView>()
          where TView : ContentControl, new();
    }
}

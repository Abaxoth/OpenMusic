﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Client.WPF.Player;
using OM.Service.Dto;

namespace OM.Client.WPF.Services.Abstractions
{
    public interface IOpenMusicPlayer
    {
        bool IsPlaying { get; }

        RepeatType RepeatType { get; set; }

        double Volume { get; set; }

        SongPreviewDto CurrentSong { get; }

        bool IsShuffleEnabled { get; set; }

        public long CurrentMillisecond { get; }

        public long TotalMilliseconds { get; }


        event Action OnPause;
        event Action OnPlay;
        event Action OnSetSong;

        Task SetPlaylistAsync(SongPlaylist playlist);

        Task SkipMediaAsync();

        Task BackMediaAsync();

        void Play();

        void Pause();
    }
}

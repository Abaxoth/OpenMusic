﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Service.Dto;

namespace OM.Client.WPF.Services.Abstractions
{
    internal interface ISongRepository
    {
        Task<string> ProvideSongFileAsync(SongPreviewDto song);
    }
}

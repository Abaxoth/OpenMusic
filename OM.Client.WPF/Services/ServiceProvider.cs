﻿using System;
using Ninject;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Services.Settings;

namespace OM.Client.WPF.Services
{
    internal class ServiceProvider: IServiceProvider
    {
        public object GetService(Type serviceType)
        {
           return ServiceConfiguration.Kernel.Get(serviceType);
        }
    }
}

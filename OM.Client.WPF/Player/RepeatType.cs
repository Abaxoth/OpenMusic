﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Client.WPF.Player
{
    public enum RepeatType
    {
        /// <summary>
        /// Repeat disabled
        /// </summary>
        Disabled = 0,
        /// <summary>
        /// Repeat single song
        /// </summary>
        Single = 1,
        /// <summary>
        /// Repeat playlist
        /// </summary>
        Playlist = 2,
    }
}

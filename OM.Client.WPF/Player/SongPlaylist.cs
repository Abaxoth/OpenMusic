﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Core.Extensions;
using OM.Service.Domain.Models;
using OM.Service.Dto;

namespace OM.Client.WPF.Player
{
    public class SongPlaylist
    {
        public List<SongPreviewDto> Songs { get; set; } = new();

        public SongPreviewDto GetNextOrNull(SongPreviewDto songDto)
        {
            var index = Songs.IndexOf(songDto);
            return Songs[index + 1];
        }

        public SongPreviewDto this[int index] => Songs[index];

        public bool IsLastSong(SongPreviewDto song)
        {
            var index = Songs.IndexOf(song);
            return Songs.Count == index + 1;
        }

        public void Shuffle()
        {
            Songs.Shuffle();
        }

        public SongPlaylist Clone()
        {
            return new SongPlaylist()
            {
                Songs = Songs.Select(x => x with { }).ToList(),
            };
        }
    }
}

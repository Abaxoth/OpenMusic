﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using OM.Client.App.Services.Abstractions;
using OM.Client.WPF.Services.Abstractions;
using OM.Core.Extensions;
using OM.Service.Dto;

namespace OM.Client.WPF.Player
{
    internal class OpenMusicPlayer : IOpenMusicPlayer
    {
        private readonly MediaPlayer _player;
        private readonly SemaphoreSlim _semaphore = new(1, 1);

        public OpenMusicPlayer(ISongRepository songRepository, IMp3Helper mp3Helper)
        {
            SongRepository = songRepository;
            Mp3Helper = mp3Helper;
            _player = new MediaPlayer();
            _player.MediaEnded += OnMediaEnded;
        }

        private async void OnMediaEnded(object sender, EventArgs e)
        {
            switch (RepeatType)
            {
                case RepeatType.Disabled:
                    var nextSong = Playlist.GetNextOrNull(CurrentSong);
                    if (nextSong == null) break;

                    await SetSongInternalAsync(nextSong);
                    Play();
                    break;
                case RepeatType.Single:
                    await SetSongInternalAsync(CurrentSong);
                    Play();
                    break;

                case RepeatType.Playlist:
                    var isLast = Playlist.IsLastSong(CurrentSong);
                    if (!isLast) break;

                    if (IsShuffleEnabled) Playlist.Shuffle();
                    var firstSong = Playlist.Songs.First();
                    await SetSongInternalAsync(firstSong);
                    Play();
                    break;
            }

            OnSongEnded?.Invoke();
        }

        protected IMp3Helper Mp3Helper { get; }
        protected ISongRepository SongRepository { get; }

        public double Volume
        {
            get => _player.Volume;
            set => _player.Volume = value;
        }
        public SongPreviewDto CurrentSong { get; private set; }

        public SongPlaylist Playlist { get; private set; }

        public RepeatType RepeatType { get; set; }


        public bool IsShuffleEnabled { get; set; }

        public string SongFilePath { get; private set; }

        public long CurrentMillisecond
        {
            get => (long)_player.Position.TotalMilliseconds;
            set => _player.Position = TimeSpan.FromMilliseconds(value);
        }

        public long TotalMilliseconds
        {
            get
            {
                if (Mp3Helper.TryNormalizeMp3FilePath(SongFilePath, out var songFile))
                    return (long)Mp3Helper.GetFileDuration(songFile).TotalMilliseconds;

                return 0;
            }
        }

        public bool IsPlaying { get; private set; }

        public event Action OnSongEnded;
        public event Action OnPause;
        public event Action OnPlay;
        public event Action OnSetSong;

        public async Task SetPlaylistAsync(SongPlaylist playlist)
        {
            if (!playlist.Songs.Any())
                throw new Exception($"Playlist have no songs");

            CloseSong();

            if (IsShuffleEnabled)
                playlist.Shuffle();

            Playlist = playlist;
            await SetSongInternalAsync(playlist.Songs.First());
        }
        public void Pause()
        {
            _player.Pause();
            IsPlaying = false;
            OnPause?.Invoke();
        }

        public void Play()
        {
            _player.Play();
            IsPlaying = true;
            OnPlay?.Invoke();
        }

        public async Task SkipMediaAsync()
        {
            using (await _semaphore.JoinAsync())
            {
                var nextSong = Playlist.GetNextOrNull(CurrentSong);
                if (nextSong == null) return;

                await PlaySongInternalAsync(nextSong);
            }
        }

        public async Task BackMediaAsync()
        {
            using (await _semaphore.JoinAsync())
            {
                var index = Playlist.Songs.IndexOf(CurrentSong);
                if (index < 0) index = 0;

                var song = Playlist.Songs[index];
                await PlaySongInternalAsync(song);
            }
        }

        public async Task SetSongAsync(SongPreviewDto song)
        {
            await SetSongInternalAsync(song);
        }

        private async Task SetSongInternalAsync(SongPreviewDto song)
        {
            CloseSong();
            CurrentSong = song;
            SongFilePath = await SongRepository.ProvideSongFileAsync(song);
            var filePath = Mp3Helper.NormalizeMp3FilePath(SongFilePath);
            _player.Open(new Uri(filePath));
            OnSetSong?.Invoke();
        }

        private async Task PlaySongInternalAsync(SongPreviewDto song)
        {
            await SetSongInternalAsync(song);
            Play();
        }

        private void CloseSong()
        {
            _player.Close();
            IsPlaying = false;
        }
   
    }
}

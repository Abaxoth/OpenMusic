﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using OM.Client.WPF.Player;

namespace OM.Client.WPF.Controls
{
    public class ButtonRepeat: ButtonEllipse
    {
        public ButtonRepeat()
        {
            this.Click += OnClick;
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            switch (Repeat)
            {
                case RepeatType.Disabled:
                    Repeat = RepeatType.Playlist;
                    break;
                case RepeatType.Playlist:
                    Repeat = RepeatType.Single;
                    break;
                case RepeatType.Single:
                    Repeat = RepeatType.Disabled;
                    break;
            }
        }

        public bool IsSingleRepeatMode
        {
            get => (bool)GetValue(IsSingleRepeatModeProperty);
            set => SetValue(IsSingleRepeatModeProperty, value);
        }

        public bool IsPlaylistRepeatMode
        {
            get => (bool)GetValue(IsPlaylistRepeatModeProperty);
            set => SetValue(IsPlaylistRepeatModeProperty, value);
        }

        public bool IsDisabledRepeatMode
        {
            get => (bool)GetValue(IsDisabledRepeatModeProperty);
            set => SetValue(IsDisabledRepeatModeProperty, value);
        }

        public RepeatType Repeat
        {
            get => (RepeatType)GetValue(RepeatProperty);
            set => SetValue(RepeatProperty, value);
        }

        public static readonly DependencyProperty RepeatProperty =
            DependencyProperty.Register(nameof(Repeat),
                typeof(RepeatType),
                typeof(ButtonRepeat),
                new PropertyMetadata(null));

        public static readonly DependencyProperty IsSingleRepeatModeProperty =
            DependencyProperty.Register(nameof(IsSingleRepeatMode),
                typeof(bool),
                typeof(ButtonRepeat),
                new PropertyMetadata(null));

        public static readonly DependencyProperty IsPlaylistRepeatModeProperty =
            DependencyProperty.Register(nameof(IsPlaylistRepeatMode),
                typeof(bool),
                typeof(ButtonRepeat),
                new PropertyMetadata(null));

        public static readonly DependencyProperty IsDisabledRepeatModeProperty =
            DependencyProperty.Register(nameof(IsDisabledRepeatMode),
                typeof(bool),
                typeof(ButtonRepeat),
                new PropertyMetadata(null));
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OM.Client.WPF.Controls
{
    internal class ButtonShuffle : ButtonEllipse
    {
        public ButtonShuffle()
        {
            this.Click += OnClick;
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            IsShuffle = !IsShuffle;
        }

        public bool IsShuffle
        {
            get => (bool)GetValue(IsShuffleProperty);
            set => SetValue(IsShuffleProperty, value);
        }


        public static readonly DependencyProperty IsShuffleProperty =
            DependencyProperty.Register(nameof(IsShuffle),
                typeof(bool),
                typeof(ButtonShuffle),
                new PropertyMetadata(null));
    }
}


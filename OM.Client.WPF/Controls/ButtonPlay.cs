﻿using System.Windows;

namespace OM.Client.WPF.Controls
{
    public class ButtonPlay: ButtonEllipse
    {
        public bool IsPlay
        {
            get => (bool)GetValue(IsPlayProperty);
            set => SetValue(IsPlayProperty, value);
        }


        public static readonly DependencyProperty IsPlayProperty =
            DependencyProperty.Register(nameof(IsPlay),
                typeof(bool),
                typeof(ButtonPlay),
                new PropertyMetadata(null));
    }
}

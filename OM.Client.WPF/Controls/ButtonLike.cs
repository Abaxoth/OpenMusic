﻿using System.Windows;

namespace OM.Client.WPF.Controls
{
    public class ButtonLike : ButtonEllipse
    {
        public ButtonLike()
        {
            this.Click += OnClick;
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            IsLiked = !IsLiked;
        }

        public bool IsLiked
        {
            get => (bool)GetValue(IsLikedProperty);
            set => SetValue(IsLikedProperty, value);
        }


        public static readonly DependencyProperty IsLikedProperty =
            DependencyProperty.Register(nameof(IsLiked),
                typeof(bool),
                typeof(ButtonLike),
                new PropertyMetadata(null));
    }
}

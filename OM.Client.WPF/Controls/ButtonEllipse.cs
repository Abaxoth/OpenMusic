﻿using System.Windows;
using System.Windows.Controls;

namespace OM.Client.WPF.Controls
{
    public class ButtonEllipse: Button
    {
        public CornerRadius CornerRadius
        {
            get => (CornerRadius)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register(nameof(CornerRadius),
                typeof(CornerRadius),
                typeof(ButtonEllipse),
                new PropertyMetadata(null));
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Client.WPF.Settings
{
    public static class AppSettings
    {
        static AppSettings()
        {
            EnsureDirectoryExists(DataDirectory);
            EnsureDirectoryExists(LogDirectory);
            EnsureDirectoryExists(SongMetaDataDirectory);
            EnsureDirectoryExists(SongsDirectory);
        }

        public static string DataDirectory => "Data";
        public static string LogDirectory => Path.Combine(DataDirectory, "Logs");
        public static string AppConfigPath => Path.Combine(DataDirectory, "AppConfig.json");

        public static string SongMetaDataDirectory => Path.Combine(DataDirectory, "Songs Meta Data");

        public static string SongsDirectory => Path.Combine(DataDirectory, "Songs");

        public static void EnsureDirectoryExists(string directoryPath)
        {
            if (Directory.Exists(directoryPath)) return;
            Directory.CreateDirectory(directoryPath);
        }

    }
}

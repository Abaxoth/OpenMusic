﻿using System;
using Melnik.Logging.FileProvider;
using Melnik.Logging.Providers.FileProvider;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace OM.Client.WPF.Settings
{
    public static class LogSettings
    {
        static LogSettings()
        {
            LoggerFactory = new LoggerFactory();
            LoggerFactory.AddProvider(new FileLoggerProvider(new FileLoggerOptions()
            {
                LogFilePath = $"[{DateTime.Now}] OM.Client.Logs.txt",
            }));
        }

        public static ILoggerFactory LoggerFactory { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Melnik.Common.Json;

namespace OM.Client.WPF.Settings
{
    public class AppConfig: JsonFile<AppConfig>
    {
        private static AppConfig _main;
        public static AppConfig Main
        {
            get
            {
                if (_main != null) return _main;
                var configFilePath = AppSettings.AppConfigPath;

                if (File.Exists(configFilePath))
                {
                    _main = LoadFromFile(configFilePath);
                    return _main;
                }
                else
                {
                    _main = GetDefault(configFilePath);
                    return _main;
                }
            }
        }

        protected override AppConfig GetDefaultInternal()
        {
            return new AppConfig();
        }
    }
}

﻿using System.Windows.Controls;

namespace OM.Client.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for SongPreviewControl.xaml
    /// </summary>
    public partial class SongPreviewControl : UserControl
    {
        public SongPreviewControl()
        {
            InitializeComponent();
        }
    }
}

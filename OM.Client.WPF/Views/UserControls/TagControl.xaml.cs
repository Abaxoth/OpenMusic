﻿using System.Windows.Controls;

namespace OM.Client.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for TagControl.xaml
    /// </summary>
    public partial class TagControl : UserControl
    {
        public TagControl()
        {
            InitializeComponent();
        }
    }
}

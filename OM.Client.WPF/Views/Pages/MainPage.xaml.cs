﻿using System.Windows;
using System.Windows.Input;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Helpers;
using OM.Client.WPF.Services.Settings;
using OM.Client.WPF.ViewModels.Pages;
using OM.Client.WPF.ViewModels.UserControls;
using OM.Core.Extensions;

namespace OM.Client.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Window
    {
        public MainPage()
        {
            InitializeComponent();
            SourceInitialized += OnSourceInitializedAsync;
        }

        internal MainViewModel ViewModel => (MainViewModel)DataContext;

        private async void OnSourceInitializedAsync(object sender, System.EventArgs e)
        {
            ViewModel.PlayerControl = ServiceConfiguration.Kernel.GetService<PlayerViewModel>();
            Player.DataContext = ViewModel.PlayerControl;
        }

        private void UIElement_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            ViewModel.SearchMusicAsync.Execute(this);
        }
    }
}

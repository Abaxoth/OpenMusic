﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Client.WPF.Architecture
{
    [AttributeUsage(AttributeTargets.Class)]
    internal class ViewModelAttribute: Attribute
    {
        public ViewModelAttribute(Type viewType)
        {
            ViewType = viewType;
        }

        public Type ViewType { get; }
    }
}

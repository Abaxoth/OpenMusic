﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using DevExpress.Mvvm;
using Ninject;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Helpers;
using OM.Client.WPF.Services.Abstractions;
using OM.Client.WPF.Services.Settings;
using OM.Client.WPF.ViewModels.Pages;
using OM.Client.WPF.Views.Pages;
using OM.Core.Extensions;

namespace OM.Client.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void AppStartup(object sender, StartupEventArgs e)
        {
            //MainWindow = new TestPage();
            //MainWindow.Show();

            //return;

            var viewInitializer = ServiceConfiguration.Kernel.Get<IViewInitializer>();
            MainWindow = viewInitializer.InitializeView<MainPage>();
            MainWindow.NotNull("Can't initialize main page").Show();
        }

        
    }
}

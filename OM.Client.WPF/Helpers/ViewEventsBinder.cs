﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using OM.Client.WPF.ViewModels.Events;

namespace OM.Client.WPF.Helpers
{
    public static class ViewEventsConfiguration
    {
        public static void RegisterEvents(Window window)
        {
            if (window.DataContext is ILoadView viewModel)
            {
                window.Loaded += (_, _) => viewModel.OnLoadAsync();
            }

            if (window.DataContext is ICloseView viewModel2)
            {
                window.Closed+= (_, _) => viewModel2.OnCloseAsync();
                viewModel2.CloseEvent += window.Close;
            }
        }

    }
}

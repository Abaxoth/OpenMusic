﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Provider.Abstractions;
using OM.Service.Dto;

namespace OM.Client.WPF.Helpers
{
    public static class MockRepository
    {
        public static SongPreviewDto GetSongPreviewDto()
        {
            return new()
            {
                Author = "Morhen",
                Name = "Money",
                PreviewImageUri = @"https://m.media-amazon.com/images/I/81sRyioXZsL._SS500_.jpg",
                SongId = 1,
                SourceType = MusicProviderType.OpenMusic,
                Url = "N/A",
            };
        }

        public static string Mp3FilePath =>
            @"C:\Users\Supra\Desktop\Open Music\Bass.prod.by Nurjan      Morgenshtern - Уфф деньги.mp3";
    }
}

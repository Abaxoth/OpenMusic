﻿using System;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using DevExpress.Mvvm;
using Microsoft.WindowsAPICodePack.Shell;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Helpers;
using OM.Client.WPF.Services.Abstractions;
using OM.Client.WPF.ViewModels.Pages;
using OM.Client.WPF.Views.UserControls;
using OM.Service.Dto;

namespace OM.Client.WPF.ViewModels.UserControls
{
    [ViewModel(typeof(PlayerControl))]
    internal class PlayerViewModel : BaseViewModel<PlayerViewModel>, IDisposable
    {
        private Timer _timer;

        public PlayerViewModel()
        {

        }

        public PlayerViewModel(IOpenMusicPlayer player)
        {
            Player = player;
            Initialize();
        }



        public IOpenMusicPlayer Player { get; }

        private void OnUpdateFrame(object sender, ElapsedEventArgs e)
        {
            UpdateView();
        }

        public ICommand PlayAction => new DelegateCommand(() =>
        {
            if (Player.IsPlaying)
            {
                Player.Pause();
            }
            else
            {
                Player.Play();
            }

            UpdateView();
        });

        public ICommand SkipAction => new AsyncCommand(async () =>
        {
           await Player.SkipMediaAsync();
        });

        public ICommand LikeAction => new AsyncCommand(async () =>
        {
            MessageBox.Show("N/I");
        });

        public ICommand BackAction => new AsyncCommand(async () =>
        {
            await Player.BackMediaAsync();
        });

        private static TimeSpan GetVideoDuration(string filePath)
        {
            using var shell = ShellObject.FromParsingName(filePath);
            var prop = shell.Properties.System.Media.Duration;
            var t = (ulong)prop.ValueAsObject;
            return TimeSpan.FromTicks((long)t);
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        private void UpdateView()
        {
            OnPropertyChanged(nameof(Player));
        }

        private void Initialize()
        {
            _timer = new Timer(1) { Enabled = true };
            _timer.Elapsed += OnUpdateFrame;
            _timer.Start();
        }
    }
}

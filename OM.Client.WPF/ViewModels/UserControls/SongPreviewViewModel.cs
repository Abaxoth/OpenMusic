﻿using System;
using System.Windows.Input;
using DevExpress.Mvvm;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Helpers;
using OM.Client.WPF.Player;
using OM.Client.WPF.Services.Abstractions;
using OM.Client.WPF.ViewModels.Pages;
using OM.Client.WPF.Views.UserControls;
using OM.Service.Domain.Models;
using OM.Service.Dto;

namespace OM.Client.WPF.ViewModels.UserControls
{
    [ViewModel(typeof(SongPreviewControl))]
    public class SongPreviewViewModel : BaseViewModel<SongPreviewViewModel>
    {
        public SongPreviewViewModel()
        {
            SongPreview = MockRepository.GetSongPreviewDto();
        }

        public SongPreviewViewModel(IOpenMusicPlayer player)
        {
            Player = player;
            Player.OnPlay += UpdateView;
            Player.OnPause += UpdateView;
            Player.OnSetSong += UpdateView;
        }

        protected IOpenMusicPlayer Player { get; }

        public SongPlaylist SongPlaylist { get; set; } 

        public SongPreviewDto SongPreview { get; set; }

        public bool IsPlaying
        {
            get
            {
                if (Player == null || SongPreview == null) return false;

                if (Player.CurrentSong != SongPreview) return false;
                return Player?.IsPlaying == true;
            }
        }

        /// <summary>
        /// Play or stop btn click
        /// </summary>
        public ICommand ActionAsync => new AsyncCommand(async () =>
        {
            try
            {
                if (Player.CurrentSong?.SongId != SongPreview.SongId)
                    await Player.SetPlaylistAsync(SongPlaylist.Clone());

                if (IsPlaying)
                {
                    Player.Pause();
                }
                else
                {
                    Player.Play();
                }

                UpdateView();
            }
            catch (Exception ex)
            {
                AppHelper.ShowException(ex);
            }
        });

        private void UpdateView()
        {
            OnPropertyChanged(nameof(Player));
            OnPropertyChanged(nameof(IsPlaying));
        }
    }
}

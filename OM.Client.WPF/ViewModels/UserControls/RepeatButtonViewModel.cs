﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.ViewModels.Pages;
using OM.Client.WPF.Views.UserControls;

namespace OM.Client.WPF.ViewModels.UserControls
{
    [ViewModel(typeof(RepeatButtonControl))]
    internal class RepeatButtonViewModel : BaseViewModel<RepeatButtonViewModel>, IDisposable
    {

        public void Dispose()
        {
        }
    }
}

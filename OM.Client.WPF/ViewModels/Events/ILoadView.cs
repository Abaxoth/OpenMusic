﻿using System.Threading.Tasks;

namespace OM.Client.WPF.ViewModels.Events
{
    public interface ILoadView
    {
        Task OnLoadAsync();
    }
}

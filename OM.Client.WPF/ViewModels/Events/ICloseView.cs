﻿using System;
using System.Threading.Tasks;
using System.Windows;

namespace OM.Client.WPF.ViewModels.Events
{
    public interface ICloseView 
    {
        Task OnCloseAsync();

        event Action CloseEvent;
    }
}

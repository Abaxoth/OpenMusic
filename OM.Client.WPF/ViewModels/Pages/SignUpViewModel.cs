﻿using System.Windows.Input;
using DevExpress.Mvvm;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Helpers;
using OM.Client.WPF.Views.Pages;
using OM.Service.Domain.API.Requests;
using OM.Service.Domain.API.Responses;

namespace OM.Client.WPF.ViewModels.Pages
{
    [ViewModel(typeof(SignUpPage))]
    public class SignUpViewModel: BaseViewModel<SignUpViewModel>
    {
        public string Username
        {
            get => GetPropValue<string>(nameof(Username));
            set => SetPropValue(nameof(Username), value);
        }
        public string Email
        {
            get => GetPropValue<string>(nameof(Email));
            set => SetPropValue(nameof(Email), value);
        }
        public string Password
        {
            get => GetPropValue<string>(nameof(Password));
            set => SetPropValue(nameof(Password), value);
        }

        public SignUpResponse Result { get; private set; }

        public ICommand SignUpAsync => new AsyncCommand(async () =>
        {
            //var signUpRequest = new SignUpRequest()
            //{
            //    Username = Username,
            //    Password = Password,
            //    Email = Email,
            //};

            //Result = await AppHttpClient.SignUpAsync(signUpRequest);
            //if (!Result.IsOk)
            //{
            //    AppHelper.ShowResponseError(Result.Error);
            //    return;
            //}

            //Close();
        });
    }
}

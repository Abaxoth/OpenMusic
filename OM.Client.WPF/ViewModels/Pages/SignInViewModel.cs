﻿using System;
using System.Windows.Input;
using DevExpress.Mvvm;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Helpers;
using OM.Client.WPF.Views.Pages;
using OM.Service.Domain.API.Requests;
using OM.Service.Domain.API.Responses;

namespace OM.Client.WPF.ViewModels.Pages
{
    [ViewModel(typeof(SignInPage))]
    public class SignInViewModel: BaseViewModel<SignInViewModel>
    {
        public string Username
        {
            get => GetPropValue<string>(nameof(Username));
            set => SetPropValue(nameof(Username), value);
        }
       
        public string Password
        {
            get => GetPropValue<string>(nameof(Password));
            set => SetPropValue(nameof(Password), value);
        }

        public SignInResponse Result { get; private set; }

        public ICommand SignInAsync => new AsyncCommand(async () =>
        {
            try
            {
                //var request = new SignInRequest()
                //{
                //    Username = Username,
                //    Password = Password,
                //};

                //Result = await AppHttpClient.SignInAsync(request);
                //if (!Result.IsOk)
                //{
                //    AppHelper.ShowResponseError(Result.Error);
                //    return;
                //}

                //Close();

            }catch(Exception ex)
            {
                AppHelper.ShowException(ex);
            }
        });
    }
}

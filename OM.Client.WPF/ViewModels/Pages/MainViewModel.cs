﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using OM.Client.App.Http;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Helpers;
using OM.Client.WPF.Player;
using OM.Client.WPF.Services.Abstractions;
using OM.Client.WPF.ViewModels.UserControls;
using OM.Client.WPF.Views.Pages;
using OM.Client.WPF.Views.UserControls;
using OM.Provider.Abstractions;
using OM.Service.Dto;
using OM.Core.Extensions;

namespace OM.Client.WPF.ViewModels.Pages
{
    [ViewModel(typeof(MainPage))]
    internal class MainViewModel : BaseViewModel<MainViewModel>
    {
        public MainViewModel(IViewInitializer viewInitializer, IServiceProvider serviceProvider,
            AppHttpClient httpClient)
        {
            ViewInitializer = viewInitializer;
            HttpClient = httpClient;
            ServiceProvider = serviceProvider;
        }

        protected IServiceProvider ServiceProvider { get; }
        protected IViewInitializer ViewInitializer { get; }
        protected AppHttpClient HttpClient { get; }

        public MainViewModel()
        {
            InitializePreview();
        }

        private void InitializePreview()
        {
            for (int i = 0; i < 8; i++)
            {
                var vm = new SongPreviewViewModel();
                var control = new SongPreviewControl { DataContext = vm };
                SongPreviewControls.Add(control);
            }
        }


        public string SearchQuery
        {
            get => GetPropValue<string>(nameof(SearchQuery));
            set => SetPropValue(nameof(SearchQuery), value);
        }

        public PlayerViewModel PlayerControl
        {
            get => GetPropValue<PlayerViewModel>(nameof(PlayerControl));
            set => SetPropValue(nameof(PlayerControl), value);
        }

        public string Tittle { get; set; } = "Open Music";

        public ObservableCollection<SongPreviewControl> SongPreviewControls { get; set; } = new();

        public ICommand SearchMusicAsync => new AsyncCommand(async () =>
        {
            try
            {
                using var waitCursor = new WaitCursor();

                var filter = new SearchSongsFilterDto()
                {
                    ProviderTypes = new[] { MusicProviderType.YoutubeMusic },
                    SearchQuery = SearchQuery,
                };

                var songPreviews = await HttpClient.SearchSongsAsync(filter);
                var controls = songPreviews
                    .Select(x=> CreateSongPreviewControl(x, songPreviews));

                SongPreviewControls.Clear();
                foreach (var songPreviewControl in controls)
                    SongPreviewControls.Add(songPreviewControl);
            }
            catch (Exception ex)
            {
                AppHelper.ShowException(ex);
            }
        });

        public ICommand SignUpAsync => new DelegateCommand(() =>
        {
            var viewModel = new SignUpViewModel();
            ViewInitializer.ShowByViewModel(viewModel, true);

            if (viewModel.Result == null) return;

            MessageBox.Show($"You are sign up");

        });

        public ICommand SignInAsync => new AsyncCommand(async () =>
        {
            var viewModel = new SignInViewModel();
            ViewInitializer.ShowByViewModel(viewModel, true);
            if (viewModel.Result == null) return;

            MessageBox.Show($"You are sign in");
        });

        private void Display(string text)
        {
            MessageBox.Show(text);
        }

        public ICommand Test => new DelegateCommand(() =>
        {
            var preview = MockRepository.GetSongPreviewDto();
            var mp3FilePath = MockRepository.Mp3FilePath;
           // PlayerControl.SetSongAsync(preview, mp3FilePath);
        });

        public override Task OnLoadAsync()
        {
            //var preview = MockRepository.GetSongPreviewDto();
            //var mp3FilePath = MockRepository.Mp3FilePath;

            //var vm = new SongPreviewViewModel();
            //vm.SongPreview = new CurrentSong()
            //{
            //    Url = $"{AppHttpClient.ServiceApi}/";
            //};

            //PlayerControl.SetSongAsync(preview, mp3FilePath);
            return Task.CompletedTask;
        }

        private SongPreviewControl CreateSongPreviewControl(SongPreviewDto dto,
            IReadOnlyCollection<SongPreviewDto> allSongs)
        {
            var index = allSongs.IndexOf(x => x.SongId == dto.SongId);
            var songs = allSongs.ToList().GetRange(index, allSongs.Count - index);
            var playlist = new SongPlaylist()
            {
                Songs = songs,
            };

            var vm = ServiceProvider.GetService<SongPreviewViewModel>();
            vm.SongPlaylist = playlist;
            vm.SongPreview = dto;
            return ViewInitializer.InitializeView<SongPreviewControl>(vm);
        }

        private void OnPreviewSongStop(SongPreviewDto obj)
        {
           
        }

        private void OnPreviewSongPlay(SongPreviewDto obj)
        {
        }
    }
}

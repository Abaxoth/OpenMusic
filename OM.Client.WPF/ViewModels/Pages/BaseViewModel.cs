﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Extensions.Logging;
using OM.Client.WPF.Architecture;
using OM.Client.WPF.Settings;
using OM.Client.WPF.ViewModels.Events;

namespace OM.Client.WPF.ViewModels.Pages
{
    public class BaseViewModel : DependencyObject
    {

    }

    public class BaseViewModel<TParentViewModel> : BaseViewModel,
        INotifyPropertyChanged, ILoadView, ICloseView
    {
        private readonly Dictionary<object, object> _propName2PropValue = new();
        public event PropertyChangedEventHandler PropertyChanged;

        public BaseViewModel()
        {
            Logger = LogSettings.LoggerFactory.CreateLogger<TParentViewModel>();
        }

        public event Action CloseEvent;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected ILogger Logger { get; }

        protected T GetPropValue<T>(string propertyName)
        {
            if(!_propName2PropValue.ContainsKey(propertyName))
                _propName2PropValue.Add(propertyName, null);

            var value = _propName2PropValue[propertyName];
            if (value == null)
                return default;

            return (T) value;
        }

        protected void SetPropValue<T>(string propertyName, T value)
        {
            _propName2PropValue[propertyName] = value;
            OnPropertyChanged(propertyName);
        }

        public virtual async Task OnLoadAsync()
        {
        }

        public virtual async Task OnCloseAsync()
        {
        }

        protected void Close()
        {
           CloseEvent?.Invoke();
        }
    }
}

﻿using System;
using Melnik.Puppeteer;
using PuppeteerSharp;

namespace OM.Provider.Common
{
    public class BrowserContext : IDisposable
    {
        private readonly BrowserPoolItem _item;

        public BrowserContext(BrowserPoolItem poolItem)
        {
            _item = poolItem;
        }

        public Browser Browser => _item.Browser;
        public Page MainPage { get; set; }

        public void Dispose()
        {
            _item.Dispose();
        }
    }
}

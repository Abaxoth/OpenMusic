﻿namespace OM.Provider.Common.Helpers
{
    public class JsonExtractOptions
    {
        public bool AutoNormalizeSpecSymbols { get; set; } = true;
    }
}

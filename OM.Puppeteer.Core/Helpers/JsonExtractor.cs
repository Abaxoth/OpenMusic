﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace OM.Provider.Common.Helpers
{
    public class JsonExtractor
    {
        private int _openedSymbolsCount = 0;
        private int _strMarkupsCount = 0;
        private string _currentText = "";

        public JsonExtractor(string text)
        {
            var extractOptions = new JsonExtractOptions();
            Text = NormalizeText(text, extractOptions);
        }

        public JsonExtractor(string text, JsonExtractOptions extractOptions)
        {
            Text = NormalizeText(text, extractOptions);
        }

        public string Text { get; }
        private bool IsReading => _openedSymbolsCount > 0;
        private bool IsString => _strMarkupsCount % 2 != 0;

        public static JObject ExtractFirstJson(string text)
        {
            return ExtractFirstJson(text, new JsonExtractOptions());
        }

        public static JObject ExtractFirstJson(string text, JsonExtractOptions options)
        {
            var jObjects = ExtractAllJson(text);
            if (jObjects.Count == 0)
                throw new Exception("Can't find any json in string");

            return jObjects.First();
        }

        public static List<JObject> ExtractAllJson(string text)
        {
            return ExtractAllJson(text, new JsonExtractOptions());
        }

        public static List<JObject> ExtractAllJson(string text, JsonExtractOptions extractOptions)
        {
            var extractor = new JsonExtractor(text, extractOptions);
            var jObjects = extractor.ExtractAllJson();
            return jObjects;
        }

        public List<JObject> ExtractAllJson()
        {
            var result = new List<JObject>();

            foreach (var symbol in Text)
            {
                if (symbol == '{')
                    _openedSymbolsCount++;

                if (symbol == '}')
                {
                    _openedSymbolsCount--;
                    if (IsReading == false)
                    {
                        _currentText += symbol;
                    }
                }

                if (IsReading)
                {
                    var symbolToWrite = symbol;
                    if (symbolToWrite == '\"')
                        _strMarkupsCount++;


                    if (IsString)
                    {
                        _currentText += symbolToWrite;
                    }
                    else
                    {
                        if (IsValidSymbol(symbolToWrite))
                        {
                            _currentText += symbolToWrite;
                        }
                        else
                        {
                            StopReading();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(_currentText))
                    {
                        StopReading();
                    }
                    else
                    {
                        if (TryParseToJObject(_currentText, out var jObject))
                            result.Add(jObject);

                        StopReading();
                    }
                }
            }

            return result;
        }

        private static string NormalizeText(string text, JsonExtractOptions options)
        {
            if (options.AutoNormalizeSpecSymbols)
            {
                text = text.Replace(@"\\", @"\");
            }

            return text;
        }

        private bool TryParseToJObject(string jsonStr, out JObject result)
        {
            try
            {
                result = JObject.Parse(jsonStr);
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        private void StopReading()
        {
            if (_currentText.Contains("M1IpPAREgRw"))
            {
                var a1 = _currentText.Count(item => item == '{');
                var a21 = _currentText.Count(item => item == '}');

                var a = 100;
            }

            _strMarkupsCount = 0;
            _openedSymbolsCount = 0;
            _currentText = "";
        }

        private bool IsValidSymbol(char symbol)
        {
            if (symbol == '(') return false;
            if (symbol == ')') return false;

            return true;
        }
    }
}

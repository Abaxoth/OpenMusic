﻿using System;
using OM.Provider.Abstractions;

namespace OM.Provider.Common.Helpers
{
    public static class ProviderTypeConverter
    {
        public static MusicProviderType ToSourceType(string uri)
        {
            if (uri.Contains("youtube.com"))
                return MusicProviderType.YoutubeMusic;

            if (uri.Contains("soundcloud.com"))
                return MusicProviderType.SoundCloud;

            throw new Exception($"Can't determinate provider type by uri: {uri}");
        }
    }
}

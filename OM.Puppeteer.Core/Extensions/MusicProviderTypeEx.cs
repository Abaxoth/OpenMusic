﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OM.Provider.Abstractions;

namespace OM.Provider.Common.Extensions
{
    public static class MusicProviderTypeEx
    {
        public static IEnumerable<MusicProviderType> All(this MusicProviderType item)
        {
            return Enum.GetValues(item.GetType()).Cast<MusicProviderType>();
        }
    }
}

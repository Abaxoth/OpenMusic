﻿using Ninject;
using OM.Client.App;
using OM.Core.Extensions;

namespace OM.Client.Tests
{
    internal static class ServiceProvider
    {
        static ServiceProvider()
        {
            Kernel = new StandardKernel(new ClientAppServiceModule());
        }

        public static IKernel Kernel { get; }

        public static TService GetService<TService>()
        {
            return Kernel.GetService<TService>();
        }
    }
}

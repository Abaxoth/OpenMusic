﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DevExpress.Mvvm;
using OM.Provider.Tester.Annotations;
using OM.SoundCloud;
using OM.Youtube;

namespace OM.Provider.Tester.ViewModels
{
    public class MainViewModel : DependencyObject, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string DownloadUrl { get; set; }

        public ICommand DownloadAsync => new AsyncCommand(async () =>
        {
            //try
            //{
            //    var type = GetProviderType();

            //    switch (type)
            //    {
            //        case MusicProvider.Youtube:
            //            var provider = new YoutubeMusicProvider();
            //            var filePath = "Youtube.mp4";
            //            await provider.DownloadYoutubeVideo(DownloadUrl, filePath);
            //            Process.Start(filePath);
            //            break;

            //        case MusicProvider.SoundCloud:
            //            var soundCoud = new SoundCloudWeb();
            //            var soundCloudFile = "SoundCloud.mp3";
            //            var songBytes = await soundCoud.DownloadSongAsync(DownloadUrl);
            //            File.WriteAllBytes(soundCloudFile, songBytes);
            //            Process.Start(soundCloudFile);
            //            break;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //}

        });

        public ICommand DisplayPreview => new AsyncCommand(async () =>
        {
            //var type = GetProviderType();

            //switch (type)
            //{
            //    case MusicProvider.Youtube:
            //        var provider = new YoutubeMusicProvider();
            //        var filePath = "YoutubePreview.jpg";
            //        provider.DownloadVideoPreview(DownloadUrl, filePath);
            //        Process.Start(filePath);
            //        break;
            //}
        });

    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OM.Provider.Music.Abstractions;
using OM.Provider.Music.Models;
using OM.Service.API.Controllers.Common;
using OM.Service.Application.CommandHandlers.Abstractions;
using OM.Service.Application.Readers.Abstractions;
using OM.Service.Dto;

namespace OM.Service.API.Controllers
{
    [ApiController]
    [Route("Api/Songs")]
    public class SongsController : DefaultController<SongsController>
    {
        private readonly ISongRepositoryLogic _songRepositoryLogic;
        private readonly ISongCommandHandler _songCommandHandler;
        private readonly IMusicService _musicService;

        public SongsController(ILogger<SongsController> logger, ISongRepositoryLogic songRepositoryLogic,
            ISongCommandHandler songCommandHandler,
            IMusicService musicService) : base(logger)
        {
            _songRepositoryLogic = songRepositoryLogic;
            _musicService = musicService;
            _songCommandHandler = songCommandHandler;
        }

        [HttpPost]
        [Route("Search")]
        public async Task<IReadOnlyCollection<SongPreviewDto>> SearchSongsAsync(
            [FromBody] SearchSongsFilterDto request)
        {
            var findOptions = new FindSongsOptions()
            {
                ProviderTypes = request.ProviderTypes,
                SearchQuery = request.SearchQuery,
            };

            var songPreviews = await _musicService.FindSongsAsync(findOptions);
            await _songRepositoryLogic.UpdateSongsAsync(songPreviews);

            var filter = new SongsFilterDto()
            {
                Urls = songPreviews.Select(x => x.SongKey.Url),
            };

            var songs = await _songRepositoryLogic.ReadSongsAsync(filter);

            return songs.Select(x => new SongPreviewDto()
            {
                Author = x.AuthorName,
                Name = x.Name,
                PreviewImageUri = x.PreviewImageUri,
                SongId = x.Id,
                SourceType = x.ProviderType,
                Url = x.Uri,
            }).ToList();
        }

        [HttpGet]
        [Route("Check/{value}")]
        public async Task<TResult> CheckAsync<TResult>(TResult value)
        {
            return value;
        }

        [HttpGet]
        [Route("Download/{songId}")]
        public async Task<byte[]> DownloadSongByIdAsync(long songId)
        {
            return await _songCommandHandler.ReadSongContentAsync(songId);
        }
       
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace OM.Service.API.Controllers.Common
{
    public abstract class DefaultController<TController> : ControllerBase
        where TController: ControllerBase
    {
        protected DefaultController(ILogger<TController> logger)
        {
            Logger = logger;
        }

        protected ILogger<TController> Logger { get; }


        [HttpGet("IsEnabled")]
        public bool IsEnabled()
        {
            return true;
        }
    }
}

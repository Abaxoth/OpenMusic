﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OM.Service.API.Controllers.Common;
using OM.Service.Application.CommandHandlers.Abstractions;
using OM.Service.Domain.API.Base;
using OM.Service.Domain.API.Requests;
using OM.Service.Domain.API.Responses;

namespace OM.Service.API.Controllers
{
    [ApiController]
    [Route("Api/Users")]
    public class UsersController : DefaultController<UsersController>
    {
        private readonly IAuthCommandHandler _authCommandHandler;

        public UsersController(ILogger<UsersController> logger, IAuthCommandHandler authCommandHandler) :
            base(logger)
        {
            _authCommandHandler = authCommandHandler;
        }


        [HttpPost]
        [Route("SignUp")]
        public async Task<SignUpResponse> SignUpAsync([FromBody] SignUpRequest request)
        {
            try
            {
                return await _authCommandHandler.SignUpAsync(request);
            }
            catch (Exception ex)
            {
                return BaseResponse.Failed<SignUpResponse>(ex);
            }
        }

        [HttpPost]
        [Route("SignIn")]
        public async Task<SignInResponse> SignInAsync([FromBody] SignInRequest request)
        {
            try
            {
                return await _authCommandHandler.SignInAsync(request);
            }
            catch (Exception ex)
            {
                return BaseResponse.Failed<SignInResponse>(ex);
            }
        }

    }
}

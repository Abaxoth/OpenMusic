﻿using System;
using OM.Provider.Common;

namespace OM.Provider.SoundCloud
{
    [Serializable]
    public class SoundCloudProviderOptions
    {
        public BrowserContext BrowserContext { get; set; }
    }
}

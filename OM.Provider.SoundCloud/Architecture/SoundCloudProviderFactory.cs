﻿using OM.Provider.Abstractions;

namespace OM.Provider.SoundCloud.Architecture
{
    public class SoundCloudProviderFactory: IMusicProviderFactory
    {
        private readonly SoundCloudProviderOptions _providerOptions;

        public SoundCloudProviderFactory(SoundCloudProviderOptions providerOptions)
        {
            _providerOptions = providerOptions;
        }

        public IMusicProvider CreateProvider()
        {
            return new SoundCloudProvider(_providerOptions);
        }
    }
}

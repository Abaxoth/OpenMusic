﻿using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using OM.Provider.Common.Helpers;

namespace OM.Provider.SoundCloud.Models
{
    internal class SongHtmlPage
    {
        public SongHtmlPage(string html)
        {
            Html = html;
        }

        public string Html { get; }

        public string SongInfoRequestUrl => ExtractTrackUrl(Html);

        private string ExtractTrackUrl(string html)
        {
            var jsonItems = JsonExtractor.ExtractAllJson(html);
            var matched = jsonItems.Where(item => item.ToString().Contains("transcodings")).ToList();
            if (!matched.Any())
                throw new Exception($"Can't find valid json");

            var json = matched.First();

            var url = json["data"][0]["media"]["transcodings"][0]["url"].Value<string>();
            return url;
        }
    }
}

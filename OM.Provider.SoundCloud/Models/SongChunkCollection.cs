﻿using System;
using System.Collections.Generic;

namespace OM.Provider.SoundCloud.Models
{
    internal class SongChunkCollection
    {
        public SongChunkCollection(string chunksResponse)
        {
            Chunks = ExtractChunks(chunksResponse);
        }


        public List<SongChunk> Chunks { get; }

        private List<SongChunk> ExtractChunks(string chunkResponse)
        {
            var result = new List<SongChunk>();

            var isReading = false;
            var url = "";

            foreach (var symbol in chunkResponse)
            {
                if (symbol == ',')
                {
                    isReading = true;
                    continue;
                }

                if (symbol == '#')
                {
                    isReading = false;

                    try
                    {
                        result.Add(new SongChunk(new Uri(url.Trim())));
                    }
                    catch {}

                    url = "";
                }

                if (isReading)
                    url += symbol;
            }

            return result;
        }
    }
}

﻿using System;

namespace OM.Provider.SoundCloud.Models
{
    internal class SongChunk
    {
        public SongChunk(Uri chunkAddress)
        {
            Address = chunkAddress;
        }

        public Uri Address { get; }
    }
}

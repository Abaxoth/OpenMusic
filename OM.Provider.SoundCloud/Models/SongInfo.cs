﻿namespace OM.Provider.SoundCloud.Models
{
    internal class SongInfo
    {
        public SongInfo(string url)
        {
            Url = url;
        }

        public string Url { get; }
    }
}

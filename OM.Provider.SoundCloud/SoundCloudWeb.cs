﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using OM.Provider.SoundCloud.Models;
using Placer.WebCore;
using ProxySupport;

namespace OM.Provider.SoundCloud
{
    internal class SoundCloudWeb : BaseWeb
    {
        public const string DefaultUserAgent =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36";
        public const string DefaultAcceptEncodingHeader = "";
        public const string DefaultAcceptLanguageHeader = "en-US,en;q=0.9";
        public const string DefaultClientId = "jdhj09mFfEyGQTY6Y3vHmfHisv9NpPUD";

        public SoundCloudWeb()
            : base(DefaultUserAgent, DefaultAcceptLanguageHeader,
                DefaultAcceptEncodingHeader, null)
        {

        }

        public SoundCloudWeb(string userAgent, string acceptLanguage,
            string acceptEncoding, IProxySettings proxySettings)
            : base(userAgent, acceptLanguage, acceptEncoding, proxySettings)
        {

        }


        public async Task<byte[]> DownloadSongAsync(string songUrl)
        {
            var html = await GetSongHtmlAsync(songUrl);
            var track = await GetSongInfoAsync(html.SongInfoRequestUrl,
                DefaultClientId);

            var chunkCollection = await GetSongChunksAsync(track);
            var fileBytes = new List<byte>();

            foreach (var chunk in chunkCollection.Chunks)
            {
                var chunkBytes = await DownloadChunkDataAsync(chunk);
                fileBytes.AddRange(chunkBytes);
            }

            return fileBytes.ToArray();
        }

        private async Task<byte[]> DownloadChunkDataAsync(SongChunk songChunk)
        {
            var message = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = songChunk.Address,
            };

            message.Headers.Add("Connection", "keep-alive");
            message.Headers.Add("User-Agent", UserAgent);
            message.Headers.Add("Accept-Encoding", AcceptEncoding);
            message.Headers.Add("Accept-Language", AcceptLanguage);
            message.Headers.Add("Accept", "*/*");
            message.Headers.Add("Referer", "https://soundcloud.com/");
            message.Headers.Add("Sec-Fetch-Site", "cross-site");
            message.Headers.Add("Sec-Fetch-Mode", "cors");
            message.Headers.Add("Sec-Fetch-Dest", "empty");

            using var client = new HttpClient();
            var response = await client.SendAsync(message);
            var content = await response.Content.ReadAsByteArrayAsync();
            return content;
        }

        private async Task<SongChunkCollection> GetSongChunksAsync(SongInfo songInfo)
        {
            var message = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(songInfo.Url)
            };

            message.Headers.Add("Connection", "keep-alive");
            message.Headers.Add("User-Agent", UserAgent);
            message.Headers.Add("Accept-Encoding", AcceptEncoding);
            message.Headers.Add("Accept-Language", AcceptLanguage);
            message.Headers.Add("Accept", "*/*");
            message.Headers.Add("Referer", "https://soundcloud.com/");
            message.Headers.Add("Sec-Fetch-Site", "cross-site");
            message.Headers.Add("Sec-Fetch-Mode", "cors");
            message.Headers.Add("Sec-Fetch-Dest", "empty");

            using var client = new HttpClient();
            var response = await client.SendAsync(message);
            var content = await response.Content.ReadAsStringAsync();
            return new SongChunkCollection(content);
        }

        private async Task<SongInfo> GetSongInfoAsync(string songInfoUrl, string clientId)
        {
            var message = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{songInfoUrl}?client_id={clientId}")
            };

            message.Headers.Add("Connection", "keep-alive");
            message.Headers.Add("User-Agent", UserAgent);
            message.Headers.Add("Accept-Encoding", AcceptEncoding);
            message.Headers.Add("Accept-Language", AcceptLanguage);
            message.Headers.Add("Accept", "*/*");
            message.Headers.Add("Referer", "https://soundcloud.com/");
            message.Headers.Add("Sec-Fetch-Site", "cross-site");
            message.Headers.Add("Sec-Fetch-Mode", "cors");
            message.Headers.Add("Sec-Fetch-Dest", "empty");

            using var client = new HttpClient();
            var response = await client.SendAsync(message);
            var content = await response.Content.ReadAsStringAsync();
            var url = JObject.Parse(content)["url"].Value<string>();
            return new SongInfo(url);
        }

        private async Task<SongHtmlPage> GetSongHtmlAsync(string songUrl)
        {
            var message = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(songUrl)
            };

            message.Headers.Add("Connection", "keep-alive");
            message.Headers.Add("User-Agent", UserAgent);
            message.Headers.Add("Accept-Encoding", AcceptEncoding);
            message.Headers.Add("Accept-Language", AcceptLanguage);
            message.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            message.Headers.Add("Referer", "https://soundcloud.com/");
            message.Headers.Add("Sec-Fetch-Site", "cross-site");
            message.Headers.Add("Sec-Fetch-Mode", "cors");
            message.Headers.Add("Sec-Fetch-Dest", "empty");

            using var client = new HttpClient();
            var response = await client.SendAsync(message);
            var responseBytes = await response.Content.ReadAsStreamAsync();
            var doc = new HtmlDocument();
            doc.Load(responseBytes);
            return new SongHtmlPage(doc.Text);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using OM.Provider.Abstractions;
using OM.Provider.Abstractions.Models;
using OM.Provider.Common;
using PuppeteerSharp;
using BrowserContext = OM.Provider.Common.BrowserContext;
using Page = PuppeteerSharp.Page;

namespace OM.Provider.SoundCloud
{
    internal class SoundCloudBrowser : IDisposable
    {
        public SoundCloudBrowser(BrowserContext browserContext)
        {
            Browser = browserContext.Browser;
            MainPage = browserContext.MainPage;
        }

        public Browser Browser { get; }
        public Page MainPage { get; }

        public async Task<IEnumerable<ISongPreview>> FindSongs(string searchQuery)
        {
            var url = $"https://soundcloud.com/search?q={searchQuery}";

            var task = MainPage.WaitForResponseAsync(item =>
                item.Url.Contains("search?q") && item.Url.Contains("variant_ids"));

            await MainPage.GoToAsync(url);
            var response = await task;

            var body = await response.TextAsync();
            return ConvertToSongPreviewData(body);
        }

        private IEnumerable<ISongPreview> ConvertToSongPreviewData(string response)
        {
            var result = new List<SongPreviewData>();
            var json = JObject.Parse(response);

            foreach (var songItem in json["collection"])
            {
                if (songItem["kind"].Value<string>().ToLower() != "track") continue;

                var previewUri = songItem["artwork_url"].Value<string>();
                var name = songItem["title"].Value<string>();
                var link = songItem["permalink_url"].Value<string>();
                var author = songItem["user"]["username"].Value<string>();

                if (string.IsNullOrEmpty(previewUri))
                    previewUri = songItem["user"]["avatar_url"].Value<string>();

                var previewData = new SongPreviewData
                {
                    SongKey = new SongKey() { Id = link, Url = link },
                    Name = name,
                    PreviewImageUri = previewUri,
                    SongUrl = link,
                    Author = author,
                    SourceType = MusicProviderType.SoundCloud,
                };

                result.Add(previewData);
            }

            return result;
        }

        public void Dispose()
        {
            Browser?.Dispose();
            MainPage?.Dispose();
        }
    }
}

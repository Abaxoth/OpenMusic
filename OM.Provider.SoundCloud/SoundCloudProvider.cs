﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OM.Provider.Abstractions;
using OM.Provider.Abstractions.Models;

namespace OM.Provider.SoundCloud
{
    internal class SoundCloudProvider : IMusicProvider
    {
        private readonly SoundCloudProviderOptions _options;

        public SoundCloudProvider(SoundCloudProviderOptions options)
        {
            _options = options;
        }

        public async Task<IEnumerable<ISongPreview>> FindSongsAsync(string searchQuery)
        {
            var browser = new SoundCloudBrowser(_options.BrowserContext);
            var songs = await browser.FindSongs(searchQuery);
            return songs;
        }

        public Task<byte[]> DownloadSongAsync(string songUrl)
        {
            using var web = new SoundCloudWeb();
            var data = web.DownloadSongAsync(songUrl);
            return data;
        }

        public void Dispose()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Service.Domain.API.Base
{
    public class ResponseError
    {
        public Exception Exception { get; set; }
        public string ErrorMsg { get; set; }

    }
}

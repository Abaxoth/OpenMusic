﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Service.Domain.API.Base
{
    public abstract class BaseResponse
    {
        public bool IsOk => Error == null;
       
        public ResponseError Error { get; set; }


        public static TResponse Failed<TResponse>(Exception ex, string errorMsg = "")
            where TResponse : BaseResponse, new()
        {
            return new()
            {
                Error = new ResponseError()
                {
                    Exception = ex,
                    ErrorMsg = string.IsNullOrWhiteSpace(errorMsg) ? ex.Message : errorMsg,
                }
            };
        }

        public static TResponse ValidationFailed<TResponse>(string errorMessage)
            where TResponse : BaseResponse, new()
        {
            return new()
            {
                Error = new ResponseError()
                {
                    Exception = new Exception(errorMessage),
                    ErrorMsg = errorMessage,
                }
            };
        }

    }
}

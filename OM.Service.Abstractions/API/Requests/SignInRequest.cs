﻿using OM.Service.Domain.API.Base;

namespace OM.Service.Domain.API.Requests
{
    public class SignInRequest: BaseRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

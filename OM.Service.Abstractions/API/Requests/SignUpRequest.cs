﻿using System.ComponentModel.DataAnnotations;
using OM.Service.Domain.API.Base;

namespace OM.Service.Domain.API.Requests
{
    public class SignUpRequest : BaseRequest
    {
        [StringLength(16, MinimumLength = 4)]
        public string Username { get; set; }
        [StringLength(16, MinimumLength = 4)]
        public string Password { get; set; }

        [StringLength(100, MinimumLength = 4)]
        public string Email { get; set; }
    }
}

﻿using OM.Service.Domain.API.Base;

namespace OM.Service.Domain.API.Responses
{
    public class SignInResponse: BaseResponse
    {
        public long UserId { get; set; }
        public string SessionToken { get; set; }
    }
}

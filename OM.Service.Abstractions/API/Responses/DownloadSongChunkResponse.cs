﻿using System;
using OM.Service.Domain.API.Base;

namespace OM.Service.Domain.API.Responses
{
    [Serializable]
    public class DownloadSongChunkResponse: BaseResponse
    {
        public long TotalBytes { get; set; }
        public byte[] Data { get; set; }
    }
}

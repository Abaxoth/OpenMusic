﻿using OM.Service.Domain.API.Base;

namespace OM.Service.Domain.API.Responses
{
    public class SignUpResponse : BaseResponse
    {
        public string SessionToken { get; set; }
        public long UserId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using OM.Service.Domain.Models.Common;

namespace OM.Service.Domain.Models
{
    [Serializable]
    public class PlayList: BaseMongoModel<long>
    {
        public Guid OwnerUserId { get; set; }

        public List<Guid> SongIds { get; set; } = new();
    }
}

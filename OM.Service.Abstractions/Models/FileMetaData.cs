﻿using OM.Service.Domain.Models.Common;

namespace OM.Service.Domain.Models
{
    public class FileMetaData : BaseMongoModel<long>
    {
        public string NameId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using OM.Service.Domain.Models.Common;

namespace OM.Service.Domain.Models
{
    [Serializable]
    public class UserProfile : BaseMongoModel<long>
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime SignUpTime { get; set; }
        public DateTime LastSignInTime { get; set; }
        public List<Guid> FollowedPlaylistIds { get; set; }
        public List<Guid> OwnedPlaylistIds { get; set; }
    }
}

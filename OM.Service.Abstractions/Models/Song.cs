﻿using System;
using OM.Provider.Abstractions;
using OM.Service.Domain.Models.Common;

namespace OM.Service.Domain.Models
{
    [Serializable]
    public class Song : BaseMongoModel<long>
    {
        public string Uri { get; set; }
        public string Name { get; set; }
        public string AuthorName { get; set; }
        public string PreviewImageUri { get; set; }
        public MusicProviderType ProviderType { get; set; }
        public long? FileId { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public DateTime CreationTime { get; set; }
    }
}

﻿namespace OM.Service.Domain.Models.Common
{
    public class BaseMongoModel<TId>: BaseMongoModel
    {
        public TId Id { get; set; }
    }

    public class BaseMongoModel
    {
    }
}

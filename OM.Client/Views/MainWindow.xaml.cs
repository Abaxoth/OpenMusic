﻿using System.Windows;
using System.Windows.Controls;
using Melnik.Logging.Gui;
using Melnik.Logging.Gui.Extensions;
using OM.Client.Architecture;
using OM.Client.UserControls;
using OM.Client.ViewModels.Events;

namespace OM.Client.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            SourceInitialized += MainWindow_SourceInitialized;
            Closed += MainWindow_Closed;
        }

        private void MainWindow_Closed(object sender, System.EventArgs e)
        {
            if (DataContext is IClosedView view)
            {
                view.OnClosed();
            }
        }

        private void MainWindow_SourceInitialized(object sender, System.EventArgs e)
        {
            AddRtbLogProvider();
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is ILoadView view)
            {
                await view.OnLoadAsync();
            }
        }

        private void AddRtbLogProvider()
        {
            var options = new RichTextBoxLoggerOptions(RtbLogs)
            {
                IsLogCallingMethodsSequence = false
            };

            LogSettings.LoggerFactory.AddRichTextBoxProvider(options);

        }
    }
}

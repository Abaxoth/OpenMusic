﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OM.Client.ViewModels.Events;

namespace OM.Client.Views
{
    /// <summary>
    /// Interaction logic for AddSongView.xaml
    /// </summary>
    public partial class AddSongView : Window
    {
        public AddSongView()
        {
            InitializeComponent();
            SourceInitialized += AddSongView_SourceInitialized;
        }

        private void AddSongView_SourceInitialized(object sender, EventArgs e)
        {
            if (DataContext is ICloseView view)
            {
                view.Close += Close;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DevExpress.Mvvm;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using OM.Client.Annotations;
using OM.Client.Architecture;
using OM.Client.Data;
using OM.Client.Helpers;
using OM.Client.UserControls;
using OM.Client.ViewModels.Events;
using OM.Core;
using OM.Provider.Core;
using OM.Provider.Service;
using OM.Provider.Service.Entities;
using OM.SoundCloud;
using OM.Youtube;

namespace OM.Client.ViewModels
{
    public class MainViewModel : DependencyObject, INotifyPropertyChanged, ILoadView, IClosedView
    {
        private string _songPath;
        private ProviderManager _providerManager;

        private SongCollection _songCollection;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainViewModel()
        {
            SongControls.Add(new SongControl());
        }

        public ObservableCollection<SongControl> SongControls { get; set; }
            = new ObservableCollection<SongControl>();

        public ObservableCollection<SongControl> SearchResultControls { get; set; }
            = new ObservableCollection<SongControl>();

        public ILogger Logger { get; private set; }
        public string SearchQuery { get; set; }

        public string SongPath
        {
            get => _songPath;
            set
            {
                _songPath = value;
                OnPropertyChanged(nameof(SongPath));
            }
        }

        public ICommand AddNewSong => new DelegateCommand(() =>
        {
            try
            {
                var vm = new AddSongViewModel();
                ViewShower.Show(vm, true);
                if (vm.IsOk == false) return;

                var song = vm.Song;
                _songCollection.Add(song);
                _songCollection.Save();
                UpdateView();
            }
            catch (Exception ex)
            {
                AppHelper.ShowException(ex);
            }
        });

        public ICommand YoutubeSearch => new AsyncCommand(async () =>
        {
            try
            {
                var searchOptions = new FindSongsOptions { SearchQuery = SearchQuery };
                searchOptions.SourceTypes.Add(MusicProviderType.Youtube);
                var songs = await _providerManager.FindSongsAsync(searchOptions);

                SearchResultControls.Clear();

                foreach (var song in songs)
                {
                    var vm = new SongControlViewModel { Song = song };
                    vm.ListeningStarted += Vm_ListeningStartedAsync;
                    var controller = new SongControl { DataContext = vm };
                    SearchResultControls.Add(controller);
                }
            }
            catch (Exception ex)
            {
                AppHelper.ShowException(ex);
            }

        });

        public ICommand SoundCloudSearch => new AsyncCommand(async () =>
        {
            try
            {
                var searchOptions = new FindSongsOptions { SearchQuery = SearchQuery };
                searchOptions.SourceTypes.Add(MusicProviderType.SoundCloud);
                var songs = await _providerManager.FindSongsAsync(searchOptions);

                SearchResultControls.Clear();

                foreach (var song in songs)
                {
                    var vm = new SongControlViewModel { Song = song };
                    vm.ListeningStarted += Vm_ListeningStartedAsync;
                    var controller = new SongControl { DataContext = vm };
                    SearchResultControls.Add(controller);

                }
            }
            catch (Exception ex)
            {
                AppHelper.ShowException(ex);
            }

        });

        private async void Vm_ListeningStartedAsync(SongPreviewData obj)
        {
            try
            {
                SongPath = "";

                var options = new DownloadSongOptions()
                {
                    MusicProviderType = obj.SourceType,
                    SongUrl = obj.SongUrl,
                };

                var songData = await _providerManager.DownloadSongAsync(options);
                var filePath = "Song.mp3";
                File.WriteAllBytes(filePath, songData);
                SongPath = filePath;
                //Process.Start(filePath);

                //using (var stream = new MemoryStream(songData))
                //{
                //    var player = new SoundPlayer(stream);
                //    player.Play();
                //    await Task.Delay(10000);
                //}

            }
            catch (Exception ex)
            {
                AppHelper.ShowException(ex);
            }
        }

        public ICommand Test => new AsyncCommand(async () =>
        {
            //try
            //{
            //    var youtube = new YoutubeProvider();
            //    await youtube.DownloadYoutubeVideo("https://music.youtube.com/watch?v=0srSVLU31B4&list=RDAMVM0srSVLU31B4",
            //        "Youtube.mp4");

            //    var provider = new SoundCloudWeb();
            //    await provider.DownloadSongAsync("https://soundcloud.com/the_dj_music/niman-truwer-raida-skriptonit-taliya");
            //}
            //catch (Exception ex)
            //{
            //    AppHelper.ShowException(ex);
            //}
        });


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UpdateView()
        {
            SongControls.Clear();

            foreach (var song in _songCollection.Songs)
            {
                var control = new SongControl
                {
                    DataContext = new SongControlViewModel
                    {
                        Song = song.ToPreviewData(),
                    }
                };

                SongControls.Add(control);
            }
        }

        private void InitializeLogger()
        {
            Logger = LogSettings.LoggerFactory.CreateLogger("OpenMusic");
            Logger.LogDebug("Logger initialized");
        }

        private void InitializeSongCollection()
        {
            var filePath = AppSettings.SongsCollectionFilePath;

            if (SongCollection.TryLoadFromFile(filePath, out var collection))
            {
                _songCollection = collection;
                Logger.LogDebug("Songs loaded");
            }
            else
            {
                Logger.LogWarning($"Can't load songs from file: {filePath}");
                Logger.LogDebug("Default file creating...");
                _songCollection = SongCollection.GetDefault(filePath);
                Logger.LogDebug("Default file created");
            }
        }

        private async Task InitializeProviderManagerAsync()
        {
            var options = new ProviderManagerOptions { BrowsersCount = 1 };
            _providerManager = new ProviderManager(options);
            await _providerManager.InitializeAsync();
        }

        public async Task OnLoadAsync()
        {
            InitializeLogger();
            InitializeSongCollection();
            UpdateView();
            RemoveUselessFiles();
            await InitializeProviderManagerAsync();
        }

        private void RemoveUselessFiles()
        {
            ClearDirectory(AppSettings.TempDirectoryPath);

            foreach (var mediaFile in Directory.GetFiles(AppSettings.MediaDirectoryPath))
            {
                var file = _songCollection.Songs.
                    FirstOrDefault(item => item.SongFilePath == mediaFile);
                if (file == null)
                    File.Delete(mediaFile);
            }

            foreach (var previewFile in Directory.GetFiles(AppSettings.PreviewDirectoryPath))
            {
                var file = _songCollection.Songs.
                    FirstOrDefault(item => item.PreviewImageUri == previewFile);
                if (file == null)
                    File.Delete(previewFile);
            }
        }

        private void ClearDirectory(string directoryPath)
        {
            foreach (var file in Directory.GetFiles(directoryPath))
            {
                File.Delete(file);
            }
        }

        public void OnClosed()
        {
            _providerManager.Dispose();
        }
    }
}

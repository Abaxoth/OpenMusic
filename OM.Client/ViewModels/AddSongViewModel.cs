﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DevExpress.Mvvm;
using Microsoft.Win32;
using OM.Client.Annotations;
using OM.Client.Architecture;
using OM.Client.Helpers;
using OM.Client.ViewModels.Events;
using OM.Core;
using OM.Youtube;

namespace OM.Client.ViewModels
{
    public class AddSongViewModel : DependencyObject, INotifyPropertyChanged, ICloseView
    {
        private string _songName;
        private string _songUri;
        private string _songAuthor;
        private string _previewImageUri = AppSettings.DefaultPreviewImagePath;
        private string _videoFilePath;
        private MusicProviderType _musicProviderType;
        private bool _isEditable = false;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public string SongName
        {
            get => _songName;
            set
            {
                _songName = value;
                OnPropertyChanged(nameof(SongName));
            }
        }

        public string SongAuthor
        {
            get => _songAuthor;
            set
            {
                _songAuthor = value;
                OnPropertyChanged(nameof(SongAuthor));
            }
        }

        public bool IsEditable
        {
            get => _isEditable;
            set
            {
                _isEditable = value;
                OnPropertyChanged(nameof(IsEditable));
            }
        }

        public string SongUri
        {
            get => _songUri;
            set
            {
                _songUri = value;
                OnPropertyChanged(nameof(SongUri));

                PreviewImageUri = AppSettings.DefaultPreviewImagePath;
                if (File.Exists(_videoFilePath))
                    File.Delete(_videoFilePath);

                //InitializeSongInfoAsync(value);
            }
        }

        public string PreviewImageUri
        {
            get => _previewImageUri;
            set
            {
                _previewImageUri = value;
                OnPropertyChanged(nameof(PreviewImageUri));
            }
        }

        public SongMetaData Song { get; set; }
        public bool IsOk => Song != null;

        public ICommand ChangePreview => new DelegateCommand(() =>
         {
             try
             {

                 var fileDialog = new OpenFileDialog();
                 fileDialog.ShowDialog();

                 if (File.Exists(fileDialog.FileName) == false)
                     return;

                 PreviewImageUri = fileDialog.FileName;
             }
             catch (Exception ex)
             {
                 AppHelper.ShowException(ex);
             }
         });

        public ICommand AddSong => new AsyncCommand(async () =>
         {
             try
             {
                 if (string.IsNullOrWhiteSpace(SongUri))
                     throw new WarningException($"Song uri can't be empty");

                 if (string.IsNullOrWhiteSpace(SongName))
                     throw new WarningException($"Song name can't be empty");


                 if (IsOk)
                     Close.Invoke();
             }
             catch (Exception ex)
             {
                 AppHelper.ShowException(ex);
             }

         });

        //private async Task InitializeSongAsync()
        //{
        //    if (string.IsNullOrWhiteSpace(_videoFilePath))
        //        throw new Exception($"Can;t find video from path: {_videoFilePath}");

        //    var name = Path.GetFileName(_videoFilePath);
        //    var newFilePath = Path.Combine(AppSettings.MediaDirectoryPath, name);
        //    File.Move(_videoFilePath, newFilePath);

        //    await ManagePreviewImageAsync();

        //    var songMetaData = new SongMetaData()
        //    {
        //        AuthorName = SongAuthor,
        //        CreationTime = DateTime.Now,
        //        Id = Guid.NewGuid().ToString(),
        //        ListensCount = 0,
        //        Name = SongName,
        //        PreviewImageUri = PreviewImageUri,
        //        IsDefaultPreview = IsDefaultPreviewImage(PreviewImageUri),
        //        Rating = null,
        //        SongFilePath = newFilePath,
        //        SourceType = _musicProvider,
        //        Tags = GetTags(),
        //    };

        //    Song = songMetaData;
        //}

        //private async Task ManagePreviewImageAsync()
        //{
        //    var name = $"{Guid.NewGuid()}.jpg";
        //    var previewImagePath = Path.Combine(AppSettings.PreviewDirectoryPath, name);

        //    try
        //    {
        //        if (PreviewImageUri == AppSettings.DefaultPreviewImagePath) return;

        //        if (File.Exists(PreviewImageUri))
        //        {
        //            var previewName = Path.GetFileName(PreviewImageUri);
        //            var previewPath = Path.Combine(AppSettings.PreviewDirectoryPath, previewName);
        //            File.Copy(PreviewImageUri, previewPath);
        //            PreviewImageUri = previewPath;
        //            return;
        //        }

        //        var provider = new YoutubeProvider();
        //        provider.DownloadVideoPreview(SongUri, previewImagePath);
        //        PreviewImageUri = previewImagePath;
        //    }
        //    catch (Exception)
        //    {
        //        if (File.Exists(previewImagePath))
        //            File.Delete(previewImagePath);
        //        throw;
        //    }
        //}

        private bool IsDefaultPreviewImage(string previewImagePath)
        {
            var isDefaultPreview = string.IsNullOrWhiteSpace(previewImagePath) == false &&
                                   previewImagePath != AppSettings.DefaultPreviewImagePath;
            return isDefaultPreview;
        }

        //private async void InitializeSongInfoAsync(string songUri)
        //{
        //    try
        //    {
        //        IsEditable = false;

        //        switch (SongSourceConverter.ToSourceType(songUri))
        //        {
        //            case MusicProvider.File:
        //                InitializeSongInfoFromFile(songUri);
        //                break;
        //            case MusicProvider.Youtube:
        //                await InitializeSongInfoFromYoutubeAsync(songUri);
        //                break;
        //            default:
        //                throw new NotSupportedException();
        //        }
        //    }
        //    finally
        //    {
        //        IsEditable = true;
        //    }
        //}

        //private async Task InitializeSongInfoFromYoutubeAsync(string videoUrl)
        //{
        //    try
        //    {
        //        var provider = new YoutubeProvider();
        //        var name = $"{Guid.NewGuid()}.mp4";
        //        _videoFilePath = Path.Combine(AppSettings.TempDirectoryPath, name);
        //        var video = await provider.DownloadYoutubeVideo(videoUrl, _videoFilePath);
        //        SongName = Path.GetFileNameWithoutExtension(video.FullName);
        //        _musicProvider = MusicProvider.Youtube;
        //        PreviewImageUri =  provider.GetYouTubeVideoPreview(videoUrl);
        //    }
        //    catch
        //    {
        //        AppHelper.ShowInfo("Can't download this video from youtube!");
        //    }
        //}

        private void InitializeSongInfoFromFile(string filePath)
        {
            if (File.Exists(SongUri) == false)
                throw new Exception($"Can't find song: {SongUri}");

            var name = $"{Guid.NewGuid()}{Path.GetExtension(filePath)}";
            _videoFilePath = Path.Combine(AppSettings.TempDirectoryPath, name);
            File.Copy(filePath, _videoFilePath);
            SongName = Path.GetFileNameWithoutExtension(filePath);
            _musicProviderType = MusicProviderType.File;
        }

        private List<Tag> GetTags()
        {
            return new List<Tag>();
        }

        public Action Close { get; set; }
    }
}

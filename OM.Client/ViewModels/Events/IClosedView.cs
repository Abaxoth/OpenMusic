﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Client.ViewModels.Events
{
    public interface IClosedView
    {
        void OnClosed();
    }
}

﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using DevExpress.Mvvm;
using OM.Client.Annotations;
using OM.Client.Architecture;
using OM.Client.Helpers;
using OM.Client.ViewModels.Events;
using OM.Core;

namespace OM.Client.ViewModels
{
    public class SongControlViewModel : DependencyObject, INotifyPropertyChanged
    {
        private string _previewImage = AppSettings.DefaultPreviewImagePath;
        private SongPreviewData _song;

        public SongControlViewModel()
        {

        }

        public SongPreviewData Song
        {
            get => _song;
            set
            {
                _song = value;

                PreviewImage = _song.PreviewImageUri;
            }
        }

        public string PreviewImage
        {
            get => _previewImage;
            set
            {
                _previewImage = value;
                OnPropertyChanged(nameof(PreviewImage));
            }
        }

        public event Action<SongPreviewData> ListeningStarted;
        public event PropertyChangedEventHandler PropertyChanged;
        public event Action SongRemoving;


        public ICommand Play => new DelegateCommand(() =>
        {
            try
            {
                ListeningStarted?.Invoke(_song);
            }
            catch (Exception ex)
            {
                AppHelper.ShowException(ex);
            }
        });

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string NormalizePreviewPath(string previewPath)
        {
            var directoryPath = Directory.GetCurrentDirectory();
            if (previewPath.Contains(directoryPath))
                return previewPath;

            var path = Path.Combine(directoryPath, previewPath);
            return path;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OM.Client.Exceptions
{
    public class WarningException: Exception
    {
        public WarningException(string message, MessageBoxImage image) : base(message)
        {
            BoxImage = image;
        }

        public MessageBoxImage BoxImage { get; }
    }
}

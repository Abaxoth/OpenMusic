﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OM.Client.Architecture;
using OM.Core;

namespace OM.Client.Data
{
    [Serializable]
    public class SongCollection
    {
        [JsonProperty]
        private List<SongMetaData> _songs = new List<SongMetaData>();

        private SongCollection()
        {

        }

        [JsonIgnore]
        public string FilePath { get; private set; }

        [JsonIgnore]
        public IEnumerable<SongMetaData> Songs => _songs.ToArray();

        public static SongCollection LoadFromFile(string filePath)
        {
            var jsonStr = File.ReadAllText(filePath);
            var collection = JsonConvert.DeserializeObject<SongCollection>(jsonStr);
            collection.FilePath = filePath;
            return collection;
        }

        public static bool TryLoadFromFile(string filePath, out SongCollection collection)
        {
            try
            {
                collection = LoadFromFile(filePath);
                return true;
            }
            catch
            {
                collection = null;
                return false;
            }
        }

        public static SongCollection GetDefault(string filePath)
        {
            return new SongCollection()
            {
                FilePath = filePath,
            };
        }

        public void Add(SongMetaData song)
        {
            _songs.Add(song);
        }

        public void Save()
        {
            var jsonStr = JObject.FromObject(this).ToString();
            File.WriteAllText(FilePath, jsonStr);
        }
    }
}

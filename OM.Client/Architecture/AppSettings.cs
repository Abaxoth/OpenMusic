﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Client.Architecture
{
    public static class AppSettings
    {
        static AppSettings()
        {
            EnsureDirectoryExists(DataDirectoryPath);
            EnsureDirectoryExists(MediaDirectoryPath);
            EnsureDirectoryExists(TempDirectoryPath);
            EnsureDirectoryExists(PreviewDirectoryPath);
        }

        public static string DataDirectoryPath => "Data";
        public static string MediaDirectoryPath => "Media";
        public static string SongsCollectionFilePath => Path.Combine(DataDirectoryPath, "Save.json");
        public static string TempDirectoryPath => "Temp";
        public static string PreviewDirectoryPath => "Previews";
        public static string DefaultPreviewImagePath = "../Resources/DefaultMusicPreview.jpg";

        private static void EnsureDirectoryExists(string directoryPath)
        {
            if (Directory.Exists(directoryPath) == false)
                Directory.CreateDirectory(directoryPath);
        }
    }
}

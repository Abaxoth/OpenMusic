﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using OM.Client.ViewModels;
using OM.Client.Views;

namespace OM.Client.Architecture
{
    public static class ViewShower
    {
        public static void Show(object viewModel, bool showAsDialog)
        {
            if (viewModel is AddSongViewModel model)
            {
                ShowWindow(new AddSongView(), model, showAsDialog);
                return;
            }

            throw new NotSupportedException($"Undefined view model: {viewModel}");
        }

        private static void ShowWindow(Window window, object dataContext, bool showAsDialog)
        {
            window.DataContext = dataContext;
            if (showAsDialog)
                window.ShowDialog();
            else
                window.Show();
        }
    }
}

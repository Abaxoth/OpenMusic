﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Melnik.Logging.Extensions;
using Melnik.Logging.FileProvider;
using Microsoft.Extensions.Logging;

namespace OM.Client.Architecture
{
    public static class LogSettings
    {
        static LogSettings()
        {
            LoggerFactory  = new LoggerFactory();
            var options = new FileLoggerOptions {IsLogCallingMethodsSequence = false};
            LoggerFactory.AddFileProvider(options);
        }

        public static ILoggerFactory LoggerFactory { get; set; } = new LoggerFactory();
    }
}

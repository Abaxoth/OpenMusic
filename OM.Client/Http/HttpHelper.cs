﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OM.Provider.Abstractions.Models;
using OM.Service.Domain.Requests;
using OM.Service.Dto;

namespace OM.Client.Http
{
    public static class HttpHelper
    {
        private static string _serviceApiHost = "";

        public static async Task<IEnumerable<ISongPreview>> FindSongsAsync(SearchSongsFilterDto filter)
        {
            var url = $"{_serviceApiHost}/Api/Songs/Search";
            return await PostAsync<IEnumerable<ISongPreview>>(url, filter);
        }

        public static async Task<T> GetAsync<T>(string requestUri)
        {
            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestUri);
                var response = await client.SendAsync(request);
                var str = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(str);
            }
        }

        public static async Task<T> PostAsync<T>(string requestUri, object body)
        {
            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Post, requestUri)
                {
                    Content = new StringContent(JObject.FromObject(body).ToString())
                };

                var response = await client.SendAsync(request);
                var responseStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(responseStr);
            }
        }
    }
}

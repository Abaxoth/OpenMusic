﻿using System.Linq;

namespace OM.Service.Mongo.Helpers
{
    internal static class Mapper
    {
        public static TResult To<TResult, TBase>(TBase model)
            where TResult : TBase, new()
        {
            var modelProperties = typeof(TBase).GetProperties();
            var newModelProperties = typeof(TResult).GetProperties();
            var newModel = new TResult();

            foreach (var propertyInfo in modelProperties)
            {
                var newModelProperty = newModelProperties.
                    First(item => item.Name == propertyInfo.Name);

                var value = propertyInfo.GetValue(model);
                newModelProperty.SetValue(newModel, value);
            }

            return newModel;
        }
    }
}

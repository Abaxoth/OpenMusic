﻿using System;
using System.Collections.Generic;
using System.Text;
using OM.Provider.Abstractions;
using OM.Provider.Abstractions.Models;

namespace OM.Service.Dto
{
    [Serializable]
    public record SongPreviewDto
    {
        public long SongId { get; init; }
        public string Url { get; init; }
        public string Name { get; init; }
        public string Author { get; init; }
        public string PreviewImageUri { get; init; }
        public MusicProviderType SourceType { get; init; }

       
    }
}

﻿using System;

namespace OM.Service.Dto
{
    [Serializable]
    public class SongChunkDto
    {
        public byte[] Data { get; set; }
        public long TotalSongBytes { get; set; }
    }
}

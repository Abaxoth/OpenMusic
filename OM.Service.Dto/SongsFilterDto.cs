﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Provider.Abstractions;

namespace OM.Service.Dto
{
    public class SongsFilterDto
    {
        public IEnumerable<string> Urls { get; set; } = new string[] { };
        public IEnumerable<long> Ids { get; set; } = new long[] { };
        public IEnumerable<MusicProviderType> Providers { get; set; } = new MusicProviderType[] { };


    }
}

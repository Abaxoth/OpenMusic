﻿using System;
using OM.Provider.Abstractions;
using System.Collections.Generic;

namespace OM.Service.Dto
{
    [Serializable]
    public class SearchSongsFilterDto
    {
        public string SearchQuery { get; init; }
        public IEnumerable<MusicProviderType> ProviderTypes { get; init; }
    }
}

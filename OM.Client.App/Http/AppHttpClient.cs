﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OM.Core.Http;
using OM.Service.Domain.API.Requests;
using OM.Service.Domain.API.Responses;
using OM.Service.Dto;

namespace OM.Client.App.Http
{
    public class AppHttpClient
    {
        public const string ServiceApi = "https://localhost:44334";

        public async Task<IReadOnlyCollection<SongPreviewDto>> SearchSongsAsync(SearchSongsFilterDto searchFilter)
        {
            var url = $"{ServiceApi}/Api/Songs/Search";
            return await HttpHelper.PostAsync<IReadOnlyCollection<SongPreviewDto>>(url, searchFilter);
        }

        public async Task<byte[]> DownloadSongAsync(long songId)
        {
            var url = $"{ServiceApi}/Api/Songs/Download/{songId}";

            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url));
            using var client = new HttpClient();
            var response = await client.SendAsync(request);
            if (!response.IsSuccessStatusCode)
                throw new Exception(await response.Content.ReadAsStringAsync());

            var result = response.Content.ReadAsStringAsync().Result.Replace("\"", string.Empty);
            return Convert.FromBase64String(result);
        }

        public async Task<SignInResponse> SignInAsync(SignInRequest request)
        {
            var url = $"{ServiceApi}/api/users/signIn";
            return await HttpHelper.PostAsync<SignInResponse>(url, request);
        }

        public async Task<SignUpResponse> SignUpAsync(SignUpRequest request)
        {
            var url = $"{ServiceApi}/API/Users/SignUp";
            return await HttpHelper.PostAsync<SignUpResponse>(url, request);
        }
    }
}

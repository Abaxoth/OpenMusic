﻿using System;

namespace OM.Client.App.Services.Abstractions
{
    public interface IMp3Helper
    {
        public TimeSpan GetFileDuration(string filePath);

        public bool TryNormalizeMp3FilePath(string filePath, out string normalizedFilePath)
        {
            try
            {
                normalizedFilePath = NormalizeMp3FilePath(filePath);
                return true;
            }
            catch
            {
                normalizedFilePath = string.Empty;
                return false;
            }
        }
        public string NormalizeMp3FilePath(string filePath);
    }
}

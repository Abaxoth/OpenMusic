﻿using System;
using System.IO;
using NAudio.Wave;
using OM.Client.App.Services.Abstractions;

namespace OM.Client.App.Services
{
    internal class Mp3Helper: IMp3Helper
    {
        public TimeSpan GetFileDuration(string filePath)
        {
            filePath = NormalizeMp3FilePath(filePath);
            var reader = new MediaFoundationReader(filePath);
            return reader.TotalTime;
        }

        public string NormalizeMp3FilePath(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            return fileInfo.FullName;
        }
    }
}

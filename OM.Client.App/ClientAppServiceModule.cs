﻿using System;
using Ninject.Modules;
using OM.Client.App.Services;
using OM.Client.App.Services.Abstractions;

namespace OM.Client.App
{
    public  class ClientAppServiceModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IMp3Helper>().To<Mp3Helper>().InTransientScope();
        }
    }
}

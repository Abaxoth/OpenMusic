﻿using OM.Provider.Abstractions;

namespace OM.Provider.YouTube.Music.Architecture
{
    public class YoutubeMusicProviderFactory: IMusicProviderFactory
    {
        private readonly YoutubeMusicProviderOptions _providerOptions;

        public YoutubeMusicProviderFactory(YoutubeMusicProviderOptions options)
        {
            _providerOptions = options;
        }

        public IMusicProvider CreateProvider()
        {
            return new YoutubeMusicProvider(_providerOptions);
        }
    }
}

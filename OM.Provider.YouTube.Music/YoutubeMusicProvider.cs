﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OM.Provider.Abstractions;
using OM.Provider.Abstractions.Models;
using VideoLibrary;

namespace OM.Provider.YouTube.Music
{
    internal class YoutubeMusicProvider: IMusicProvider
    {
        private readonly YoutubeMusicProviderOptions _options;

        public YoutubeMusicProvider(YoutubeMusicProviderOptions options)
        {
            _options = options;
        }

        public async Task<YouTubeVideo> DownloadYoutubeVideo(string videoUrl, string saveAs)
        {
            // starting point for YouTube actions
            var youTube = VideoLibrary.YouTube.Default;

            // gets a Video object with info about the video
            var video = await youTube.GetVideoAsync(videoUrl);
            await File.WriteAllBytesAsync(saveAs, await video.GetBytesAsync());
            return video;
        }

        public async Task<IEnumerable<ISongPreview>> FindSongsAsync(string searchQuery)
        {
            var browser = new YoutubeMusicBrowser(_options.BrowserContext);
            var songs = await browser.FindSongsAsync(searchQuery);
            return songs;
        }

        public async Task<byte[]> DownloadSongAsync(string songUrl)
        {
            var youTube = VideoLibrary.YouTube.Default;
            var video = await youTube.GetVideoAsync(songUrl);
            var bytes = await video.GetBytesAsync();
            return bytes;
        }

        public string GetPreviewImageUrl(string videoUrl)
        {
            return GetYouTubeVideoPreview(videoUrl);
        }

        public void DownloadVideoPreview(string videoUrl, string saveAs)
        {
            var previewUrl = GetYouTubeVideoPreview(videoUrl);

            using (var web = new WebClient())
            {
                web.DownloadFile(new Uri(previewUrl), saveAs);
            }
        }

        public string GetYouTubeVideoPreview(string youTubeVideoUrl)
        {
            var web = new WebClient();
            var html = web.DownloadString(youTubeVideoUrl);
            html = html.Replace(Environment.NewLine, "").
                Replace($"\"", "'");

            var pattern = @"'thumbnail':{'thumbnails':.{'url':'(https:[^:]+)','width':\d+,'height':\d+},{'url'";
            var match = Regex.Match(html, pattern);

            return match.Groups[1].Value;

        }

        public void Dispose()
        {
        }
    }
}

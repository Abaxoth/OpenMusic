﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Melnik.Puppeteer.Exceptions;
using Newtonsoft.Json.Linq;
using OM.Provider.Abstractions;
using OM.Provider.Abstractions.Models;
using OM.Provider.Common;
using PuppeteerSharp;
using BrowserContext = OM.Provider.Common.BrowserContext;

namespace OM.Provider.YouTube.Music
{
    internal class YoutubeMusicBrowser : IDisposable
    {
        private readonly BrowserContext _browserContext;

        public YoutubeMusicBrowser(BrowserContext browserContext)
        {
            _browserContext = browserContext;
        }

        protected Browser Browser => _browserContext.Browser;
        protected Page MainPage => _browserContext.MainPage;

        public async Task<IEnumerable<ISongPreview>> FindSongsAsync(string searchQuery)
        {
            var searchUrl = $"https://music.youtube.com/search?q={searchQuery}";

            await MainPage.GoToAsync(searchUrl, WaitUntilNavigation.Networkidle0);

            var showAllBtn = await FindShowAllSongsButtonAsync();
            ClickWithDelayAsync(showAllBtn, TimeSpan.FromMilliseconds(250));

            var response = await MainPage.WaitForResponseAsync(
                item => item.Url.Contains("youtubei/v1/search"));

            var responseText = await response.TextAsync();
            var jObject = JObject.Parse(responseText);
            return ToSongPreviewDataCollection(jObject);
        }

        private async void ClickWithDelayAsync(ElementHandle btn, TimeSpan delay)
        {
            await Task.Delay(delay);
            await MainPage.ClickJsAsync(btn);
        }

        private async Task<ElementHandle> FindShowAllSongsButtonAsync()
        {
            var containers = await MainPage.
                GetElementCollectionByXpathAsync(".//div[@id='contents']/ytmusic-shelf-renderer");

            foreach (var container in containers)
            {
                var tittleNode = await container.GetFirstElementByXpathAsync(".//h2/yt-formatted-string");
                var tittle = await tittleNode.TextAsync();

                if(tittle.ToLower() != "songs") continue;

                var btn = await container.GetFirstElementByXpathAsync(".//*[text()='Show all']");
                return btn;
            }

            throw new Exception($"Can't find show all songs button");
        }

        private IEnumerable<ISongPreview> ToSongPreviewDataCollection(JToken songsJObject)
        {
            var result = new List<SongPreviewData>();

            foreach (var trackContainer in songsJObject.SelectToken("..musicShelfRenderer.contents"))
            {
                var videoId = trackContainer.SelectToken("..playNavigationEndpoint.watchEndpoint.videoId")?.Value<string>();
                var playlistId = trackContainer.SelectToken("..playNavigationEndpoint.watchEndpoint.playlistId")?.Value<string>();
                var name = trackContainer.SelectToken("..accessibilityPlayData..label")?.Value<string>();
                var url = $"https://music.youtube.com/watch?v={videoId}&list={playlistId}";
                var previewUrl = trackContainer.SelectToken("..thumbnails[-1:].url")?.Value<string>();

                var songItem = new SongPreviewData()
                {
                    SongKey = new SongKey() { Id = url, Url = url },
                    Author = "",
                    Name = name,
                    PreviewImageUri = previewUrl,
                    SongUrl = url,
                    SourceType = MusicProviderType.YoutubeMusic,
                };
                result.Add(songItem);
            }

            return result;
        }

        public void Dispose()
        {
            _browserContext.Dispose();
        }
    }
}

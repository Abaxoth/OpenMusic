﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OM.Service.DataBase.Tests.Helpers
{
    [TestClass()]
    public class MapperTests
    {
        private interface IUnit
        {
            public string Name { get; set; }
            public int Damage { get; set; }
        }

        private class Archer: IUnit
        {
            public string Name { get; set; }
            public int Damage { get; set; }
            public int Range { get; set; }
        }

        private class Warrior: IUnit
        {
            public string Name { get; set; }
            public int Damage { get; set; }
            public string WeaponName { get; set; }
        }


        [TestMethod()]
        public void ToTest()
        {
           

            Assert.Fail();
        }
    }
}
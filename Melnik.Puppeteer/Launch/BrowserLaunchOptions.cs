﻿using System.Collections.Generic;
using PuppeteerSharp;

namespace Melnik.Puppeteer.Launch
{
    public class BrowserLaunchOptions
    {
        public BrowserProxyOptions ProxyOptions { get; set; } = new BrowserProxyOptions();
        public ConnectionOptions ConnectionOptions { get; set; } = new ConnectionOptions();
        public string UserAgent { get; set; }
        public string ChromeExePath { get; set; } = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
        public bool Headless { get; set; }
        public ViewPortOptions DefaultViewPort { get; set; }
        public string[] Args { get; set; }

        public LaunchOptions ToLaunchOptions()
        {
            var args = new List<string> { "--disable-infobars" };
            args.AddRange(Args);

            if (ProxyOptions.IsEnable)
                args.Add($"--proxy-server={ProxyOptions.Ipv4}:{ProxyOptions.Port}");

            if (string.IsNullOrWhiteSpace(UserAgent) == false)
                args.Add($"--user-agent=\"{UserAgent}\"");

            if (string.IsNullOrWhiteSpace(ChromeExePath))
                ChromeExePath = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";

            var options = new LaunchOptions
            {
                Headless = Headless,
                ExecutablePath = ChromeExePath,
                DefaultViewport = DefaultViewPort,
                Args = args.ToArray(),
            };

            return options;
        }
    }
}

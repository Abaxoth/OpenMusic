﻿using System;
using System.IO;
using System.Threading.Tasks;
using PuppeteerSharp;

namespace Melnik.Puppeteer.Launch
{
    public class BrowserManager
    {
        private BrowserManager() { }

        public static async Task<Browser> LaunchBrowserAsync(BrowserLaunchOptions launchOptions)
        {
            var connectedOptions = launchOptions.ConnectionOptions;
            if (connectedOptions.ReconnectToOpenedSession)
            {
                var browser = await ConnectToOpenedSessionOrDefaultAsync(connectedOptions.SessionFile);
                if (browser != null) return browser;

                if (connectedOptions.OpenNewSessionIfFailed == false)
                    throw new Exception($"Can't connect to opened browser session");

                return await OpenNewSessionAsync(launchOptions);
            }

            return await OpenNewSessionAsync(launchOptions);
        }

        private static async Task<Browser> OpenNewSessionAsync(BrowserLaunchOptions launchOptions)
        {
            var browser = await PuppeteerSharp.Puppeteer.LaunchAsync(launchOptions.ToLaunchOptions());

            if (!launchOptions.ConnectionOptions.ReconnectToOpenedSession)
                return browser;

            var sessionFile = launchOptions.ConnectionOptions.SessionFile;
            if (string.IsNullOrWhiteSpace(sessionFile) == false)
                File.WriteAllText(sessionFile, browser.WebSocketEndpoint);

            return browser;
        }

        private static async Task<Browser> ConnectToOpenedSessionAsync(string sessionFile)
        {
            var endpoint = File.ReadAllText(sessionFile);
            var connOptions = new ConnectOptions
            {
                DefaultViewport = null,
                BrowserWSEndpoint = endpoint,
            };
            var browser = await PuppeteerSharp.Puppeteer.ConnectAsync(connOptions);
            return browser;
        }

        private static async Task<Browser> ConnectToOpenedSessionOrDefaultAsync(string sessionFile)
        {
            try
            {
                return await ConnectToOpenedSessionAsync(sessionFile);
            }
            catch
            {
                return default;
            }
        }

    }
}

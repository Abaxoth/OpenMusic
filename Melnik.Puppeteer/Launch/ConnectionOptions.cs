﻿namespace Melnik.Puppeteer.Launch
{
    public class ConnectionOptions
    {
        public string SessionFile { get; set; } = "BrowserSession.bin";
        public bool ReconnectToOpenedSession { get; set; } 
        public bool OpenNewSessionIfFailed { get; set; } = true;
    }
}

﻿namespace Melnik.Puppeteer.Launch
{
    public class BrowserProxyOptions
    {
        public int Port { get; set; }
        public string Ipv4 { get; set; }
        public bool IsEnable { get; set; }
    }
}

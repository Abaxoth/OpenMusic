﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Melnik.Puppeteer.Enums;
using PuppeteerSharp;

namespace Melnik.Puppeteer.Exceptions
{
    public static class ElementHandleEx
    {
        public static async Task<ElementHandle[]> GetElementCollectionByXpathAsync(this ElementHandle page, string xpath)
        {
            var elements = await page.XPathAsync(xpath);
            return elements;
        }

        public static async Task<ElementHandle> GetFirstElementByXpathAsync(this ElementHandle element, string xpath)
        {
            var elements = await element.XPathAsync(xpath);

            if (elements.Length == 0)
                throw new SelectorException($"Element not found by Xpath: {xpath}");

            return elements.First();
        }

        public static async Task<ElementHandle> GetFirstElementByXpathOrDefaultAsync(this ElementHandle element, string xpath)
        {
            try
            {
                return await element.GetFirstElementByXpathAsync(xpath);
            }
            catch
            {
                return default;
            }
        }

        public static async Task<string> TextAsync(this ElementHandle element)
        {
            var result = await element.ExecutionContext.Frame.EvaluateFunctionAsync($"(node)=>node.textContent", element);
            return await Task.FromResult(result.ToString());
        }

        public static async Task<string> AttributeOrDefaultAsync(this ElementHandle element, string propertyName)
        {
            try
            {
                return await element.AttributeAsync(propertyName);
            }
            catch
            {
                return null;
            }
        }

        public static async Task<string> AttributeAsync(this ElementHandle element, string propertyName)
        {
            var result = await element.ExecutionContext.Frame.EvaluateFunctionAsync($"(node)=>node.getAttribute('{propertyName}')", element);
            return result.ToString();
        }

        public static async Task<bool> IsVisibleAsync(this ElementHandle element)
        {
            var result = await element.ExecutionContext.EvaluateFunctionAsync<bool>(
                    "element => element.offsetWidth > 0 && element.offsetHeight > 0");

            return result;
        }

        internal static async Task<bool> IsMatchedForVisibleType(this ElementHandle element, VisibleType visibleType)
        {
            element.EnsureNotNull();

            switch (visibleType)
            {
                case VisibleType.Visible:
                    return await element.IsVisibleAsync();
                case VisibleType.Hidden:
                    return await element.IsVisibleAsync() == false;
                case VisibleType.Any:
                    return true;
                default:
                    throw new NotSupportedException($"VisibleType: {visibleType} not supported");
            }
        }

        internal static void EnsureNotNull(this ElementHandle handle)
        {
            if (handle == null) throw new ArgumentNullException(nameof(handle));
        }

        public static async Task SubmitAsync(this ElementHandle element)
        {
            await element.PressAsync("Enter");
        }
    }
}

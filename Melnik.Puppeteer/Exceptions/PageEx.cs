﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Melnik.Puppeteer.Args;
using PuppeteerSharp;

namespace Melnik.Puppeteer.Exceptions
{
    public static class PageEx
    {
        public static async Task WaitForHidingAsync(this Page page, string xpath, WaitOptions options)
        {
            var c = 0;
            while (c < options.WaitForMilliseconds)
            {
                var element = await page.GetFirstElementByXpathOrDefaultAsync(xpath);
                if (element == null) return;

                await Task.Delay(options.MillisecondsDelay);
                c += options.MillisecondsDelay;
            }

            throw new TimeoutException($"Timeout element not hided");
        }

        public static async Task<ElementHandle> WaitForElementByXpathAsync(this Page page, string xpath, WaitOptions options)
        {
            var count = 0;
            while (count < options.WaitForMilliseconds)
            {
                var element = await page.GetFirstElementByXpathOrDefaultAsync(xpath);
                if (element != null && await element.IsMatchedForVisibleType(options.VisibleType))
                    return await Task.FromResult(element);

                count += options.MillisecondsDelay;
                await Task.Delay(options.MillisecondsDelay);
            }

            throw new TimeoutException($"Time out but element with Xpath: {xpath} not found");
        }

        public static async Task<ElementHandle> GetFirstElementByXPathAsync(this Page page, string xpath)
        {
            var elements = await page.XPathAsync(xpath);
            if (elements.Length == 0)
                throw new Exception($"Can't find any element by Xpath: {xpath}");

            return elements.First();
        }

        public static async Task<ElementHandle> GetFirstElementByXpathOrDefaultAsync(this Page page, string xpath)
        {
            try
            {
                var elements = await page.XPathAsync(xpath);
                return elements.Length == 0 ? default : elements.First();
            }
            catch
            {
                return null;
            }
        }

        public static async Task<ElementHandle[]> GetElementCollectionByXpathAsync(this Page page, string xpath)
        {
            var elements = await page.XPathAsync(xpath);
            return elements;
        }

        public static async Task ClickJsAsync(this Page page, ElementHandle element)
        {
            element.EnsureNotNull();
            await element.EvaluateFunctionAsync("(node)=>{node.click()}", element);
        }

        public static async Task ScrollToAsync(this Page page, ElementHandle element)
        {
            element.EnsureNotNull();
            await element.HoverAsync();
        }
    }
}

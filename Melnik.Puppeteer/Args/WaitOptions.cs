﻿using Melnik.Puppeteer.Enums;

namespace Melnik.Puppeteer.Args
{
    public class WaitOptions
    {
        public WaitOptions(int waitForSeconds, int millisecondsDelay)
        {
            WaitForSeconds = waitForSeconds;
            MillisecondsDelay = millisecondsDelay;
        }

        public VisibleType VisibleType { get; set; } = VisibleType.Any;
        public int WaitForSeconds { get; }
        public int MillisecondsDelay { get; }
        public int WaitForMilliseconds => WaitForSeconds * 1000;

        public static WaitOptions WaitFor2s()
        {
            return new WaitOptions(2, 100);

        }

        public static WaitOptions WaitFor5s()
        {
            return new WaitOptions(5, 100);
        }

        public static WaitOptions WaitFor15s()
        {
            return new WaitOptions(15, 100);

        }

        public static WaitOptions WaitFor30s()
        {
            return new WaitOptions(30, 100);

        }
    }
}

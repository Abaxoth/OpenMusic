﻿using System;
using PuppeteerSharp;

namespace Melnik.Puppeteer
{
    public class BrowserPoolItem: IDisposable
    {
        private readonly BrowserPool _browserPool;

        internal BrowserPoolItem(BrowserPool pool, Browser browser)
        {
            _browserPool = pool;
            Browser = browser;
        }

        public Browser Browser { get; }

        public void Dispose()
        {
            _browserPool.Dispose(Browser);
        }
    }
}

﻿namespace Melnik.Puppeteer.Enums
{
    public enum VisibleType
    {
        Any,
        Visible,
        Hidden,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PuppeteerSharp;

namespace Melnik.Puppeteer
{
    public class BrowserPool : IDisposable
    {
        private readonly object _browserPoolKey = new object();
        private readonly object _browserWaitKey = new object();
        private readonly Dictionary<Browser, bool> _browser2IsEnable = new Dictionary<Browser, bool>();

        public BrowserPool()
        {

        }

        public void AddNew(Browser browser)
        {
            lock (_browserPoolKey)
            {
                _browser2IsEnable.Add(browser, true);
            }
        }

        public async Task<BrowserPoolItem> WaitForFreeBrowserAsync(TimeSpan maxWaitTime)
        {
            var startTime = DateTime.Now;

            while (true)
            {
                lock (_browserWaitKey)
                {
                    var dicItem = _browser2IsEnable.
                        FirstOrDefault(item => item.Value);

                    if (dicItem.Key != null)
                    {
                        var browser = dicItem.Key;
                        _browser2IsEnable[browser] = false;

                        return new BrowserPoolItem(this, browser);
                    }
                }

                await Task.Delay(100);

                if (DateTime.Now - startTime > maxWaitTime)
                    throw new Exception("Time expired");
            }

        }

        internal void Dispose(Browser browser)
        {
            lock (_browserPoolKey)
            {
                _browser2IsEnable[browser] = true;
            }
        }

        public void Dispose()
        {
            foreach (var item in _browser2IsEnable)
            {
                item.Key.Dispose();
            }

            _browser2IsEnable.Clear();
        }
    }
}

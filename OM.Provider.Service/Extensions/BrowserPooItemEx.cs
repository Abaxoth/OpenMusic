﻿using System.Linq;
using System.Threading.Tasks;
using Melnik.Puppeteer;
using OM.Provider.Common;

namespace OM.Provider.Music.Extensions
{
    internal static class BrowserPooItemEx
    {
        public static async Task<BrowserContext> ToBrowserContextAsync(this BrowserPoolItem poolItem)
        {
            var pages = await poolItem.Browser.PagesAsync();
            var page = pages.First();

            return new BrowserContext(poolItem)
            {
                MainPage = page,
            };
        }
    }
}

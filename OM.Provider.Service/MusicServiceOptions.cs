﻿using System;
using System.Collections.Generic;
using System.Text;
using Melnik.Puppeteer.Launch;
using PuppeteerSharp;

namespace OM.Provider.Music
{
    public class MusicServiceOptions
    {
        public MusicServiceOptions()
        {
            LaunchOptions = new BrowserLaunchOptions
            {
                Headless = false,
                ConnectionOptions = new ConnectionOptions()
                {
                    OpenNewSessionIfFailed = true,
                    ReconnectToOpenedSession = false,
                    
                },

                DefaultViewPort = new ViewPortOptions()
                {
                    Height = 720,
                    Width = 1080,
                },

                Args = new List<string>()
                {
                    "--no-sandbox",
                    "--disable-setuid-sandbox",
                    "--disable-infobars",
                    "--window-position=0,0",
                    "--ignore-certifcate-errors",
                    "--ignore-certifcate-errors-spki-list",
                    "--user-agent=\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36\"",
                    // "--lang=en-US,en",
                    "--lang=bn-BD,bn",
                    "--Accept-Language=en",

                }.ToArray(),
            };
        }

        public BrowserLaunchOptions LaunchOptions { get; set; } 
        public int BrowsersPoolSize { get; set; } = 1;
    }
}

﻿using System;
using OM.Provider.Abstractions;

namespace OM.Provider.Music.Models
{
    [Serializable]
    public class DownloadSongOptions
    {
        public string SongUrl { get; set; }
        public MusicProviderType MusicProviderType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using OM.Provider.Abstractions;

namespace OM.Provider.Music.Models
{
    [Serializable]
    public class FindSongsOptions
    {
        public string SearchQuery { get; set; }

        public IEnumerable<MusicProviderType> ProviderTypes { get; set; }
            = new List<MusicProviderType>();
    }
}

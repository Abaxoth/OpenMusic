﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Melnik.Puppeteer;
using Melnik.Puppeteer.Launch;
using OM.Provider.Abstractions;
using OM.Provider.Abstractions.Models;
using OM.Provider.Common;
using OM.Provider.Common.Helpers;
using OM.Provider.Music.Abstractions;
using OM.Provider.Music.Extensions;
using OM.Provider.Music.Models;
using OM.Provider.SoundCloud;
using OM.Provider.SoundCloud.Architecture;
using OM.Provider.YouTube.Music;
using OM.Provider.YouTube.Music.Architecture;
using PuppeteerSharp;
using BrowserContext = OM.Provider.Common.BrowserContext;

namespace OM.Provider.Music
{
    internal class MusicService : IMusicService
    {
        private readonly BrowserPool _browserPool = new();
        private readonly MusicServiceOptions _options;

        public MusicService(MusicServiceOptions options)
        {
            _options = options;
        }

        public async Task InitializeAsync()
        {
            for (var i = 0; i < _options.BrowsersPoolSize; i++)
            {
                var browser = await InitializeNewBrowser();
                _browserPool.AddNew(browser);
            }
        }

        public async Task<IReadOnlyCollection<ISongPreview>> FindSongsAsync(FindSongsOptions options)
        {
            var result = new List<ISongPreview>();

            foreach (var sourceType in options.ProviderTypes)
            {
                using var browserContext = await CreateBrowserContextAsync();
                var provider = CreateMusicProviderFactory(sourceType, browserContext).CreateProvider();


                var songs = await provider.FindSongsAsync(options.SearchQuery);
                result.AddRange(songs);
            }

            return result;
        }

        public async Task<byte[]> DownloadSongDataAsync(string songUrl)
        {
            var providerType = ProviderTypeConverter.ToSourceType(songUrl);
            using var browserContext = await CreateBrowserContextAsync();
            var provider = CreateMusicProviderFactory(providerType, browserContext).CreateProvider();
            var data = await provider.DownloadSongAsync(songUrl);
            return data;
        }

        public void Dispose()
        {
            _browserPool.Dispose();
        }

        private async Task<BrowserContext> CreateBrowserContextAsync()
        {
            var browser = await _browserPool.WaitForFreeBrowserAsync(TimeSpan.FromSeconds(15));
            return await browser.ToBrowserContextAsync();
        }

        private IMusicProviderFactory CreateMusicProviderFactory(MusicProviderType providerType,
            BrowserContext browserContext)
        {
            switch (providerType)
            {
                case MusicProviderType.SoundCloud:
                    return new SoundCloudProviderFactory(new SoundCloudProviderOptions()
                    {
                        BrowserContext = browserContext,
                    });

                case MusicProviderType.YoutubeMusic:
                    return new YoutubeMusicProviderFactory(new YoutubeMusicProviderOptions()
                    {
                        BrowserContext = browserContext,
                    });
                default:
                    throw new NotSupportedException($"{providerType} not supported");
            }
        }

        private async Task<Browser> InitializeNewBrowser()
        {
            var browser = await BrowserManager.LaunchBrowserAsync(_options.LaunchOptions);
            var pages = await browser.PagesAsync();
            var page = pages.First();
            await page.SetExtraHttpHeadersAsync(new Dictionary<string, string>()
            {
                {"Accept-Language", "en"},
            });

            return browser;
        }
    }
}

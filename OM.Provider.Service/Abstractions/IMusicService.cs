﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OM.Provider.Abstractions.Models;
using OM.Provider.Music.Models;

namespace OM.Provider.Music.Abstractions
{
    public interface IMusicService: IDisposable
    {
        Task<IReadOnlyCollection<ISongPreview>> FindSongsAsync(FindSongsOptions options);

        Task<byte[]> DownloadSongDataAsync(string songUrl);
    }
}

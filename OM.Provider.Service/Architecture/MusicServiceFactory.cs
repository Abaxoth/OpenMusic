﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OM.Provider.Music.Abstractions;

namespace OM.Provider.Music.Architecture
{
    public class MusicServiceFactory: IMusicServiceFactory
    {
        private readonly MusicServiceOptions _serviceOptions;

        public MusicServiceFactory(MusicServiceOptions serviceOptions)
        {
            _serviceOptions = serviceOptions;
        }

        public async Task<IMusicService> CreateMusicServiceAsync()
        {
            var service = new MusicService(_serviceOptions);
            await service.InitializeAsync();
            return service;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OM.Provider.Music.Abstractions;

namespace OM.Provider.Music.Architecture
{
    public interface IMusicServiceFactory
    {
        Task<IMusicService> CreateMusicServiceAsync();
    }
}

﻿using System;
using Microsoft.Extensions.DependencyInjection;
using OM.Provider.Music;
using OM.Provider.Music.Abstractions;
using OM.Provider.Music.Architecture;
using OM.Service.Application.CommandHandlers;
using OM.Service.Application.CommandHandlers.Abstractions;
using OM.Service.Application.Readers;
using OM.Service.Application.Readers.Abstractions;
using OM.Service.Application.Services;
using OM.Service.Application.Services.Abstractions;
using OM.Service.Application.Services.Session;
using OM.Service.Application.Services.Settings;

namespace OM.Service.Application
{
    public static class AppServiceInitializer
    {
        public static void Initialize(IServiceCollection services)
        {
            services.AddSingleton<IAppSettings, AppSettings>();
            services.AddSingleton(GetAppConfig);

            InitializeCommandHandlers(services);
            InitializeReadersLogic(services);

            services.AddSingleton<ISessionManager, SessionManager>();
            services.AddSingleton<IRepositoryContext, RepositoryContext>();
            services.AddSingleton(GetMusicService); //todo change to singleton
        }

        private static IAppConfig GetAppConfig(IServiceProvider provider)
        {
            var appSettings = provider.GetService<IAppSettings>();
            var config = new AppConfig(appSettings);
            config.Initialize();
            return config;
        }

        private static void InitializeReadersLogic(IServiceCollection services)
        {
            services.AddTransient<ISongRepositoryLogic, SongRepositoryLogic>();
            services.AddTransient<IUserRepositoryLogic, UserRepositoryLogic>();
            services.AddTransient<IFileRepositoryLogic, FileRepositoryLogic>();
        }

        private static void InitializeCommandHandlers(IServiceCollection services)
        {
            services.AddTransient<IAuthCommandHandler, AuthCommandHandler>();
            services.AddTransient<ISongCommandHandler, SongCommandHandler>();
        }

        private static IMusicService GetMusicService(IServiceProvider provider)
        {
            var config = provider.GetService<IAppConfig>();
            if (config == null)
                throw new Exception($"Service: {nameof(IAppConfig)} not found");

            var options = new MusicServiceOptions
            {
                BrowsersPoolSize = 1,
                LaunchOptions =
                {
                    ChromeExePath = config.ChromeExePath,
                    Headless = false,
                }
            };

            var factory = new MusicServiceFactory(options);
            return factory.CreateMusicServiceAsync().GetAwaiter().GetResult();
        }
    }
}

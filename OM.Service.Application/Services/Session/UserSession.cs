﻿using System;

namespace OM.Service.Application.Services.Session
{
    public class UserSession
    {
        public string Token { get; set; }
        public long UserId { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastUpdateTime { get; set; }
    }
}

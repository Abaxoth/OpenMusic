﻿using System;
using System.Collections.Generic;
using System.Linq;
using OM.Service.Application.Services.Abstractions;

namespace OM.Service.Application.Services.Session
{
    internal class SessionManager : ISessionManager
    {
        private readonly object _sessionKey = new();
        private readonly List<UserSession> _sessions = new();


        public UserSession GetUserSession(long userId)
        {
            lock(_sessionKey)
            {
                var session = _sessions.FirstOrDefault(x => x.UserId == userId);
                if (session != null) return session;

                var newSession = CreateNewSession(userId);
                _sessions.Add(newSession);
                return newSession;
            }
        }

        public void UpdateSession(long userId)
        {
            lock (_sessionKey)
            {
                var session = _sessions.FirstOrDefault(x => x.UserId == userId);
                if (session != null)
                {
                    session.LastUpdateTime = DateTime.Now;
                    return;
                }

                var newSession = CreateNewSession(userId);
                _sessions.Add(newSession);
            }
        }

        public bool TryGetUserId(string sessionToken, out long userId)
        {
            lock (_sessionKey)
            {
                var session = _sessions.FirstOrDefault(x => x.Token == sessionToken);
                userId = session?.UserId ?? 0;
                return session != null;
            }
        }

        public bool IsSessionExists(string sessionToken)
        {
            lock (_sessionKey)
            {
                return _sessions.Any(x => x.Token == sessionToken);
            }
        }

        private UserSession CreateNewSession(long userId)
        {
            var newSession = new UserSession()
            {
                Token = Guid.NewGuid().ToString(),
                UserId = userId,
                LastUpdateTime = DateTime.Now,
                CreationTime = DateTime.Now,
            };

            return newSession;
        }
    }
}

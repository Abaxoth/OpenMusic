﻿using System.IO;
using OM.Service.Application.Services.Abstractions;

namespace OM.Service.Application.Services.Settings
{
    public class AppSettings : IAppSettings
    {
        public AppSettings()
        {
            EnsureDirectoryExists(DataDirectory);
            EnsureDirectoryExists(LogDirectory);
            EnsureDirectoryExists(FilesDirectory);
        }

        public string DataDirectory => "Data";
        public string LogDirectory => Path.Combine(DataDirectory, "Logs");
        public string AppConfigPath => Path.Combine(DataDirectory, "AppConfig.json");
        public string FilesDirectory => Path.Combine(DataDirectory, "Files");

        public void EnsureDirectoryExists(string directoryPath)
        {
            if (Directory.Exists(directoryPath)) return;
            Directory.CreateDirectory(directoryPath);
        }

    }
}

﻿using System.IO;
using Melnik.Common.Json;
using MongoDB.Bson.IO;
using OM.Service.Application.Services.Abstractions;

namespace OM.Service.Application.Services.Settings
{
    public class AppConfig: IAppConfig
    {
        private readonly IAppSettings _appSettings;

        public AppConfig(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }


        public string ChromeExePath { get; set; } = @"C:\Program Files\Google\Chrome\Application\chrome.exe";
        public string MongoDataBaseName { get; set; } = "OpenMusic";
        public string MongoConnectionString { get; set; } = "mongodb://localhost:27017";

        public void Initialize()
        {
            var jsonStr = File.ReadAllText(_appSettings.AppConfigPath);
            var config = JsonHelper.FromJsonText<AppConfig>(jsonStr);

            ChromeExePath = config.ChromeExePath;
            MongoConnectionString = config.MongoConnectionString;
            MongoDataBaseName = config.MongoDataBaseName;

        }
    }
}

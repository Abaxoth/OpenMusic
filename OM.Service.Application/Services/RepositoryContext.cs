﻿using System.Threading.Tasks;
using MongoDB.Driver;
using OM.Service.Application.Services.Abstractions;
using OM.Service.Domain.Models.Common;

namespace OM.Service.Application.Services
{
    internal class RepositoryContext : IRepositoryContext
    {
        private readonly IMongoClient _mongoClient;

        public RepositoryContext(IAppConfig appConfig)
        {
            DataBaseName = appConfig.MongoDataBaseName;
            ConnectionString = appConfig.MongoConnectionString;
            _mongoClient = new MongoClient(ConnectionString);
        }

        protected string DataBaseName { get; }

        protected string ConnectionString { get; set; }

        public IMongoCollection<TDocument> GetCollection<TDocument>()
        {
            return GetCollection<TDocument>(typeof(TDocument).Name);
        }

        public IMongoCollection<TDocument> GetCollection<TDocument>(string collectionName)
        {
            return _mongoClient.GetDatabase(DataBaseName).GetCollection<TDocument>(collectionName);

        }

        public async Task<long> GetNextIdAsync<TDocument>()
            where TDocument : BaseMongoModel<long>
        {
            return await GetCollection<TDocument>().Find(x => true)
                .SortByDescending(x => x.Id)
                .Project(x => x.Id)
                .FirstOrDefaultAsync() + 1;
        }

        public long GetNextId<TDocument>()
            where TDocument : BaseMongoModel<long>
        {
            return GetNextIdAsync<TDocument>().GetAwaiter().GetResult();
        }
    }
}

﻿namespace OM.Service.Application.Services.Abstractions
{
    public interface IAppConfig
    {
        string ChromeExePath { get; }
        string MongoDataBaseName { get;  } 
        string MongoConnectionString { get; } 
    }
}

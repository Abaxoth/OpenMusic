﻿using OM.Service.Application.Services.Session;

namespace OM.Service.Application.Services.Abstractions
{
    public interface ISessionManager
    {
        UserSession GetUserSession(long userId);

        void UpdateSession(long userId);

        bool TryGetUserId(string sessionToken, out long userId);

        bool IsSessionExists(string sessionToken);
    }
}

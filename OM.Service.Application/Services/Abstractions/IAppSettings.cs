﻿namespace OM.Service.Application.Services.Abstractions
{
    public interface IAppSettings
    {
        string DataDirectory { get; }
        string LogDirectory { get; }
        string AppConfigPath { get; }

        string FilesDirectory { get; }
    }
}

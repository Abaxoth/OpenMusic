﻿using System.Threading.Tasks;
using MongoDB.Driver;
using OM.Service.Domain.Models.Common;

namespace OM.Service.Application.Services.Abstractions
{
    public interface IRepositoryContext
    {
        public IMongoCollection<TDocument> GetCollection<TDocument>();
        public IMongoCollection<TDocument> GetCollection<TDocument>(string collectionName);

        public Task<long> GetNextIdAsync<TDocument>() 
            where TDocument: BaseMongoModel<long>;

        public long GetNextId<TDocument>()
            where TDocument : BaseMongoModel<long>;

    }
}

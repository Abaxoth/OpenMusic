﻿using MongoDB.Driver;
using OM.Service.Application.Services.Abstractions;
using OM.Service.Domain.Models.Common;

namespace OM.Service.Application.Readers.Base
{
    internal class BaseRepositoryLogic
    {
        public BaseRepositoryLogic(IRepositoryContext repository)
        {
            Repository = repository;
        }

        protected IRepositoryContext Repository { get; }
    }

    internal class BaseRepositoryLogic<TMongoModel>
        where TMongoModel : BaseMongoModel
    {
        public BaseRepositoryLogic(IRepositoryContext repository)
        {
            Repository = repository;
        }

        protected IRepositoryContext Repository { get; }
        protected IMongoCollection<TMongoModel> Collection => Repository.GetCollection<TMongoModel>();
    }
}

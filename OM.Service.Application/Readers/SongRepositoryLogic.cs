﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using OM.Provider.Abstractions.Models;
using OM.Service.Application.Readers.Abstractions;
using OM.Service.Application.Readers.Base;
using OM.Service.Application.Services.Abstractions;
using OM.Service.Domain.Models;
using OM.Service.Dto;

namespace OM.Service.Application.Readers
{
    internal class SongRepositoryLogic : BaseRepositoryLogic<Song>, ISongRepositoryLogic
    {
        public SongRepositoryLogic(IRepositoryContext repository,
            IFileRepositoryLogic fileRepositoryLogic, ILogger<SongRepositoryLogic> logger) : base(repository)
        {
            FileRepositoryLogic = fileRepositoryLogic;
            Logger = logger;
        }

        protected IFileRepositoryLogic FileRepositoryLogic { get; }
        protected ILogger<SongRepositoryLogic> Logger { get; }

        public async Task<IReadOnlyCollection<Song>> ReadSongsAsync(SongsFilterDto filter)
        {
            var builder = new FilterDefinitionBuilder<Song>();
            var filterCollection = new List<FilterDefinition<Song>>();

            if (filter.Ids.Any())
                filterCollection.Add(builder
                    .Where(x => filter.Ids.Contains(x.Id)));

            if (filter.Providers.Any())
                filterCollection.Add(builder
                    .Where(x => filter.Providers.Contains(x.ProviderType)));

            if (filter.Urls.Any())
                filterCollection.Add(builder
                    .Where(x => filter.Urls.Contains(x.Uri)));

            return await Collection.Find(builder.And(filterCollection)).ToListAsync();
        }

        public async Task UpdateSongsAsync(IReadOnlyCollection<ISongPreview> songPreviews)
        {
            var urls = songPreviews.Select(x => x.SongKey.Url).ToList();
            var exists = await Collection.Find(x => urls.Contains(x.Uri)).Project(x => x.Uri).ToListAsync();

            var nextId = await Repository.GetNextIdAsync<Song>() - 1;

            var toInsertItems = songPreviews.Where(x => !exists.Contains(x.SongKey.Url))
                .Select(x => new Song()
                {
                    Id = ++nextId,
                    AuthorName = x.Author,
                    Name = x.Name,
                    PreviewImageUri = x.PreviewImageUri,
                    ProviderType = x.SourceType,
                    Uri = x.SongKey.Url,
                    CreationTime = DateTime.Now,
                    LastUpdateTime = DateTime.Now,
                }).ToList();

            if (toInsertItems.Any())
                await Collection.InsertManyAsync(toInsertItems);

            foreach (var forUpdateItem in songPreviews.Where(x => exists.Contains(x.SongKey.Url)))
            {
                var updateDefinition = Builders<Song>.Update
                    .Set(s => s.AuthorName, forUpdateItem.Author)
                    .Set(s => s.Name, forUpdateItem.Name)
                    .Set(s => s.PreviewImageUri, forUpdateItem.PreviewImageUri)
                    .Set(s => s.LastUpdateTime, DateTime.Now);

                await Collection.UpdateOneAsync(x => x.Uri == forUpdateItem.SongKey.Url, updateDefinition);
            }
        }

        public async Task<bool> IsSongExistsAsync(long songId)
        {
            var song = await Collection.Find(x => x.Id == songId)
                .FirstOrDefaultAsync();
            return song != null;
        }

        public async Task<Song> ReadSongByIdAsync(long songId)
        {
            var song = await Collection.Find(x => x.Id == songId).FirstAsync();
            return song;
        }
    }
}


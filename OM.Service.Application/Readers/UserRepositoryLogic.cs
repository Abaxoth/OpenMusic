﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using OM.Service.Application.Readers.Abstractions;
using OM.Service.Application.Readers.Base;
using OM.Service.Application.Services.Abstractions;
using OM.Service.Domain.Models;

namespace OM.Service.Application.Readers
{
    internal class UserRepositoryLogic : BaseRepositoryLogic<UserProfile>, IUserRepositoryLogic
    {
        public UserRepositoryLogic(IRepositoryContext repository) : base(repository)
        {
        }

        public async Task<long> AddNewUserAsync(string username, string email, string password)
        {
            var userProfile = new UserProfile()
            {
                Email = email,
                Username = username,
                Password = password,
                Id = await Repository.GetNextIdAsync<UserProfile>(),
                LastSignInTime = DateTime.Now,
                SignUpTime = DateTime.Now,
            };

            await Collection.InsertOneAsync(userProfile);
            return userProfile.Id;
        }
      
        public async Task<UserProfile> ReadUserByIdAsync(long userId)
        {
            var user = await Collection
                .Find(x => x.Id == userId).FirstAsync();

            return user;
        }

        public async Task<UserProfile> ReadUserByUsername(string username)
        {
            var user = await Collection
                .Find(x => x.Username == username).FirstAsync();

            return user;
        }
    }
}

﻿using System;
using System.IO;
using System.Threading.Tasks;
using Melnik.Common.IO;
using MongoDB.Driver;
using OM.Service.Application.Readers.Abstractions;
using OM.Service.Application.Readers.Base;
using OM.Service.Application.Services.Abstractions;
using OM.Service.Domain.Models;

namespace OM.Service.Application.Readers
{
    internal class FileRepositoryLogic : BaseRepositoryLogic<FileMetaData>, IFileRepositoryLogic
    {
        public FileRepositoryLogic(IRepositoryContext repository, IAppSettings appSettings) 
            : base(repository)
        {
            FilesDirectory = appSettings.FilesDirectory;
        }

        protected  string FilesDirectory { get; }

        public async Task<long> ReadFileTotalBytesAsync(long fileId)
        {

            var filePath = await GetFilePathAsync(fileId);
            return new FileInfo(filePath).Length;
        }

        public async Task<byte[]> ReadFileContentAsync(long fileId)
        {
            var filePath = await GetFilePathAsync(fileId);
            return await File.ReadAllBytesAsync(filePath);
        }

        public async Task<string> ReadFileNameAsync(long fileId)
        {
            var fileMeta = await Collection
                .Find(x => x.Id == fileId).FirstAsync();

            return fileMeta.NameId;
        }

        public async Task<long> SaveFileAsync(byte[] fileData)
        {
            var name = $"{Guid.NewGuid()}.mp3";
            var path = Path.Combine(FilesDirectory, name);
            if(File.Exists(path))
                File.Delete(path);

            await File.WriteAllBytesAsync(path, fileData);

            var fileMetaData = new FileMetaData()
            {
                Id = await Repository.GetNextIdAsync<FileMetaData>(),
                NameId = path,
            };

             await Collection.InsertOneAsync(fileMetaData);
             return fileMetaData.Id;
        }

        public async Task DeleteFileAsync(long fileId)
        {
            var path = await GetFilePathAsync(fileId);
            File.Delete(path);
            await Collection.DeleteOneAsync(x => x.Id == fileId);
        }

        public async Task<byte[]> ReadFileContentAsync(long fileId, long startBytes, 
            int bytesLength)
        {
            var filePath = await GetFilePathAsync(fileId);
            return FileHelper.GetFileBytes(filePath, startBytes, bytesLength);
        }

        private async Task<string> GetFilePathAsync(long fileId)
        {
            var metaData = await Collection
                .Find(x => x.Id == fileId).FirstAsync();

            var path = Path.Combine(FilesDirectory, metaData.NameId);
            return path;
        }
    }
}

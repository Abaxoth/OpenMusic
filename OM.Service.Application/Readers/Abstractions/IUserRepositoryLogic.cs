﻿using System.Threading.Tasks;
using OM.Service.Domain.Models;

namespace OM.Service.Application.Readers.Abstractions
{
    public interface IUserRepositoryLogic
    {
        Task<long> AddNewUserAsync(string username, string email, string password);

        Task<UserProfile> ReadUserByIdAsync(long userId);

        Task<UserProfile> ReadUserByUsername(string username);

    }

}

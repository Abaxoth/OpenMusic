﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OM.Provider.Abstractions.Models;
using OM.Service.Domain.Models;
using OM.Service.Dto;

namespace OM.Service.Application.Readers.Abstractions
{
    public interface ISongRepositoryLogic
    {
        Task<Song> ReadSongByIdAsync(long songId);

        Task UpdateSongsAsync(IReadOnlyCollection<ISongPreview> songPreviews);

         Task<bool> IsSongExistsAsync(long songId);

         Task<IReadOnlyCollection<Song>> ReadSongsAsync(SongsFilterDto filter);
    }
}

﻿using System.Threading.Tasks;

namespace OM.Service.Application.Readers.Abstractions
{
    public interface IFileRepositoryLogic
    {
        Task<long> ReadFileTotalBytesAsync(long fileId);

        Task<byte[]> ReadFileContentAsync(long fileId);

        Task<string> ReadFileNameAsync(long fileId);

        Task<long> SaveFileAsync(byte[] fileData);

        Task DeleteFileAsync(long fileId);

        Task<byte[]> ReadFileContentAsync(long fileId, long startBytes, int bytesLength);
    }
}

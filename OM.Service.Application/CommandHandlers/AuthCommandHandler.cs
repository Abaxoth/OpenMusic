﻿using System.Threading.Tasks;
using OM.Core.Extensions;
using OM.Service.Application.CommandHandlers.Abstractions;
using OM.Service.Application.CommandHandlers.Base;
using OM.Service.Application.Readers.Abstractions;
using OM.Service.Application.Services.Abstractions;
using OM.Service.Domain.API.Base;
using OM.Service.Domain.API.Requests;
using OM.Service.Domain.API.Responses;

namespace OM.Service.Application.CommandHandlers
{
    internal class AuthCommandHandler : BaseCommandHandler, IAuthCommandHandler
    {
        public AuthCommandHandler(IRepositoryContext repository, IUserRepositoryLogic userRepositoryLogic,
            ISessionManager sessionManager) : base(repository)
        {
            UserRepositoryLogic = userRepositoryLogic;
            SessionManager = sessionManager;
        }

        protected IUserRepositoryLogic UserRepositoryLogic { get; }
        protected ISessionManager SessionManager { get; }

        public async Task<SignUpResponse> SignUpAsync(SignUpRequest request)
        {
            var user = await UserRepositoryLogic.ReadUserByUsername(request.Username).OrDefaultAsync();

            if (user != null)
                return BaseResponse.ValidationFailed<SignUpResponse>("User already exists");

            var userId = await UserRepositoryLogic
                .AddNewUserAsync(request.Username, request.Email, request.Password);

            var session = SessionManager.GetUserSession(userId);

            return new SignUpResponse()
            {
                SessionToken = session.Token,
                UserId = userId,
            };
        }

        public async Task<SignInResponse> SignInAsync(SignInRequest request)
        {
            var user = await UserRepositoryLogic.ReadUserByUsername(request.Username);

            if (user == null)
                return BaseResponse.ValidationFailed<SignInResponse>("User not found");

            if (user.Password != request.Password)
                return BaseResponse.ValidationFailed<SignInResponse>("Wrong password");

            var session = SessionManager.GetUserSession(user.Id);

            return new SignInResponse()
            {
                SessionToken = session.Token,
                UserId = user.Id,
            };
        }
    }
}

﻿using System.Threading.Tasks;
using MongoDB.Driver;
using OM.Provider.Music.Abstractions;
using OM.Service.Application.CommandHandlers.Abstractions;
using OM.Service.Application.CommandHandlers.Base;
using OM.Service.Application.Readers.Abstractions;
using OM.Service.Application.Services.Abstractions;
using OM.Service.Domain.Models;

namespace OM.Service.Application.CommandHandlers
{
    internal class SongCommandHandler : BaseCommandHandler, ISongCommandHandler
    {
        public SongCommandHandler(IRepositoryContext repository, ISongRepositoryLogic songRepositoryLogic,
            IFileRepositoryLogic fileRepositoryLogic, IMusicService musicService) : base(repository)
        {
            FileRepositoryLogic = fileRepositoryLogic;
            SongRepositoryLogic = songRepositoryLogic;
            MusicService = musicService;
        }

        protected IFileRepositoryLogic FileRepositoryLogic { get; }
        protected ISongRepositoryLogic SongRepositoryLogic { get; }
        protected IMusicService MusicService { get; }

        public async Task<byte[]> ReadSongContentAsync(long songId)
        {
            var song = await SongRepositoryLogic.ReadSongByIdAsync(songId);
            if (song.FileId != null)
            {
                try
                {
                    return await FileRepositoryLogic.ReadFileContentAsync(song.FileId.Value);
                }
                catch
                {
                    // need reload file
                }
            }

            //Save file content do repository
            var songData = await MusicService.DownloadSongDataAsync(song.Uri);
            var fileId = await FileRepositoryLogic.SaveFileAsync(songData);

            var updateDef = new UpdateDefinitionBuilder<Song>()
                .Set(x => x.FileId, fileId);

            //Update song fileId
            await Repository.GetCollection<Song>().UpdateOneAsync(x => x.Id == songId, updateDef);
            return songData;
        }

    }
}

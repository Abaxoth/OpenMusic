﻿using OM.Service.Application.Services.Abstractions;

namespace OM.Service.Application.CommandHandlers.Base
{
    internal class BaseCommandHandler
    {
        public BaseCommandHandler(IRepositoryContext repository)
        {
            Repository = repository;
        }

        public IRepositoryContext Repository { get; }
    }
}

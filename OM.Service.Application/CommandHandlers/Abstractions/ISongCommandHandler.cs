﻿using System.Threading.Tasks;

namespace OM.Service.Application.CommandHandlers.Abstractions
{
    public interface ISongCommandHandler
    {
        Task<byte[]> ReadSongContentAsync(long songId);
    }
}

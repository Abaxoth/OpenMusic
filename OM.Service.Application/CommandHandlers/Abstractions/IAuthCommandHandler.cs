﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Service.Domain.API.Requests;
using OM.Service.Domain.API.Responses;

namespace OM.Service.Application.CommandHandlers.Abstractions
{
    public interface IAuthCommandHandler
    {
        Task<SignUpResponse> SignUpAsync(SignUpRequest request);
        Task<SignInResponse> SignInAsync(SignInRequest request);
    }
}

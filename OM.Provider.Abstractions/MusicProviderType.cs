﻿using System;

namespace OM.Provider.Abstractions
{
    [Serializable]
    public enum MusicProviderType
    {
        YoutubeMusic,
        SoundCloud,
        OpenMusic,
    }
}

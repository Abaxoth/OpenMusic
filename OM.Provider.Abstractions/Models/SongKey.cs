﻿using System;

namespace OM.Provider.Abstractions.Models
{
    [Serializable]
    public class SongKey: IEquatable<SongKey>
    {
        public string Id { get; set; }
        public string Url { get; set; }

        public bool Equals(SongKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && Url == other.Url;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SongKey) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Id != null ? Id.GetHashCode() : 0) * 397) ^ (Url != null ? Url.GetHashCode() : 0);
            }
        }
    }
}

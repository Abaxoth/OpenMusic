﻿namespace OM.Provider.Abstractions.Models
{
    public interface ISongPreview
    {
        SongKey SongKey { get; }
        string Name { get; }
        string Author { get; }
        string PreviewImageUri { get; }
        MusicProviderType SourceType { get; }
    }
}

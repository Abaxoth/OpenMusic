﻿using System;

namespace OM.Provider.Abstractions.Models
{
    [Serializable]
    public class SongPreviewData : ISongPreview
    {
        public SongKey SongKey { get; set; } = new SongKey();
        public string Name { get; set; }
        public string Author { get; set; }
        public string PreviewImageUri { get; set; }
        public string SongUrl { get; set; }
        public MusicProviderType SourceType { get; set; }
    }
}

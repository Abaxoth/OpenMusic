﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OM.Provider.Abstractions.Models;

namespace OM.Provider.Abstractions
{
    public interface IMusicProvider: IDisposable
    {
        Task<IEnumerable<ISongPreview>> FindSongsAsync(string searchQuery);

        Task<byte[]> DownloadSongAsync(string songUrl);
    }
}

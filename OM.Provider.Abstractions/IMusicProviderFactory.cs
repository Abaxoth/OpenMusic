﻿namespace OM.Provider.Abstractions
{
    public interface IMusicProviderFactory
    {
        IMusicProvider CreateProvider();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Provider.Abstractions;

namespace OM.Youtube.Architecture
{
    public class YoutubeMusicProviderFactory: IMusicProviderFactory
    {
        private readonly YoutubeMusicProviderOptions _providerOptions;

        public YoutubeMusicProviderFactory(YoutubeMusicProviderOptions options)
        {
            _providerOptions = options;
        }

        public IMusicProvider CreateProvider()
        {
            return new YoutubeMusicProvider(_providerOptions);
        }
    }
}

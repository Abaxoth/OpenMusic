﻿using OM.Provider.Common;

namespace OM.Youtube
{
    public class YoutubeMusicProviderOptions
    {
        public ProviderBrowserContext BrowserContext { get; set; }
    }
}

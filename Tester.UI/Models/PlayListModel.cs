﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tester.Console.Models
{
    public class PlayListModel
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public UserModel Owner { get; set; }

        public ICollection<SongModel> Songs { get; set; }
            = new List<SongModel>();
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tester.Console.Models
{
    public class UserModel
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        //public ICollection<PlayListModel> OwnedPlayLists { get; set; }
        //    = new List<PlayListModel>();
    }
}

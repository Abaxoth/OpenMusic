﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tester.Console.Models
{
    public class SongModel
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public ICollection<PlayListModel> PlayLists { get; set; }
            = new List<PlayListModel>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using NAudio.Wave;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NReco.VideoInfo;
using OM.Service.Domain.Models;
using Tester.Console;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace Tester.Console
{

    public class Program
    {
        static async Task Main(string[] args)
        {
            var filePath = new FileInfo(@"C:\C# Projects\C# Legend\OpenMusic
\OM.Client.Tests\bin\Debug\net5.0-windows\Data\Songs\Song_183.mp3").FullName;

            Log(GetMp3FileDuration(filePath));

            System.Console.ReadLine();
        }

        private static TimeSpan GetMp3FileDuration(string filePath)
        {
            var reader = new MediaFoundationReader(filePath);
            return reader.TotalTime;


            string ffMPEG = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "ffMPEG.exe");
            Process mProcess = null;

            System.IO.StreamReader SROutput = null;
            string outPut = "";

            string filepath = "D:\\source.mp4";
            string param = string.Format("-i \"{0}\"", filepath);

            System.Diagnostics.ProcessStartInfo oInfo = null;

            System.Text.RegularExpressions.Regex re = null;
            System.Text.RegularExpressions.Match m = null;
            TimeSpan? Duration = null;

            //Get ready with ProcessStartInfo
            oInfo = new System.Diagnostics.ProcessStartInfo(ffMPEG, param);
            oInfo.CreateNoWindow = true;

            //ffMPEG uses StandardError for its output.
            oInfo.RedirectStandardError = true;
            oInfo.WindowStyle = ProcessWindowStyle.Hidden;
            oInfo.UseShellExecute = false;

            // Lets start the process

            mProcess = System.Diagnostics.Process.Start(oInfo);

            // Divert output
            SROutput = mProcess.StandardError;

            // Read all
            outPut = SROutput.ReadToEnd();

            // Please donot forget to call WaitForExit() after calling SROutput.ReadToEnd

            mProcess.WaitForExit();
            mProcess.Close();
            mProcess.Dispose();
            SROutput.Close();
            SROutput.Dispose();

            //get duration

            re = new System.Text.RegularExpressions.Regex("[D|d]uration:.((\\d|:|\\.)*)");
            m = re.Match(outPut);

            if (m.Success)
            {
                //Means the output has cantained the string "Duration"
                string temp = m.Groups[1].Value;
                string[] timepieces = temp.Split(new char[] { ':', '.' });
                if (timepieces.Length == 4)
                {

                    // Store duration
                    Duration = new TimeSpan(0, Convert.ToInt16(timepieces[0]), Convert.ToInt16(timepieces[1]), Convert.ToInt16(timepieces[2]), Convert.ToInt16(timepieces[3]));
                }
            }

            return Duration.Value;
        }

      
        private static void Log(string str, params object[] msgParams)
        {
            System.Console.WriteLine(str, msgParams);
        }

        private static void Log(object str)
        {
            System.Console.WriteLine(str);
        }

    }
}

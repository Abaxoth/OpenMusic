using System;
using System.IO;
using System.Threading.Tasks;
using Melnik.Puppeteer.Launch;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OM.Provider.Music;
using OM.Provider.Music.Abstractions;
using OM.Provider.Music.Architecture;
using OM.Provider.Tests.Abstractions;
using OM.Provider.Tests.Architecture;

namespace OM.Provider.Tests
{
    [TestClass]
    public class YoutubeProviderTests
    {
        public YoutubeProviderTests()
        {
            ServiceProvider = DependencyConfiguration.Kernel;
            AppSettings = ServiceProvider.GetService<IAppSettings>();
        }

        public IServiceProvider ServiceProvider { get; }
        internal IAppSettings AppSettings { get; }

        [TestMethod]
        public async Task DownloadSong_Success()
        {
            var options = new MusicServiceOptions
            {
                BrowsersPoolSize = 1,
                LaunchOptions =
                {
                    Headless = false,
                    ChromeExePath = @"C:\Program Files\Google\Chrome\Application\chrome.exe",
                }
            };
            var factory = new MusicServiceFactory(options);
            using var service = await factory.CreateMusicServiceAsync();

            var songUrl = "https://www.youtube.com/watch?v=7JGDWKJfgxQ&list=LL&index=28&ab_channel=XXXTENTACION";

            var mediaContent = await service.DownloadSongDataAsync(songUrl);
            var filePath = Path.Combine(AppSettings.DataDirectory, "XXX.mp4");
            if (File.Exists(filePath))
                File.Delete(filePath);

            await File.WriteAllBytesAsync(filePath, mediaContent);
            Assert.AreNotEqual(mediaContent.Length, 0);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Provider.Tests.Abstractions;

namespace OM.Provider.Tests.Architecture
{
    internal class AppSettings : IAppSettings
    {
        public AppSettings()
        {
            if (!Directory.Exists(DataDirectory))
                Directory.CreateDirectory(DataDirectory);
        }

        public string DataDirectory { get; } = "Data";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using OM.Provider.Tests.Abstractions;

namespace OM.Provider.Tests.Architecture
{
    public static class DependencyModuleExtension
    {
        public static void AddProviderTests(this INinjectModule module)
        {
            module.Kernel.Bind<IAppSettings>().To<AppSettings>().InSingletonScope();
        }
    }
}

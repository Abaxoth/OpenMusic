﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;

namespace OM.Provider.Tests.Architecture
{
    internal static class DependencyConfiguration
    {
        static DependencyConfiguration()
        {
            var modules = new INinjectModule[]
            {
                new DependencyModule(),
            };

            Kernel = new StandardKernel(modules);
        }

        public static IKernel Kernel { get; }
    }
}

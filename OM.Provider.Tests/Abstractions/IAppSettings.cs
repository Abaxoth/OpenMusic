﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Provider.Tests.Abstractions
{
    internal interface IAppSettings
    {
        public string DataDirectory { get; }
    }
}

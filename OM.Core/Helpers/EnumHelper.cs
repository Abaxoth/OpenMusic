﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Core.Helpers
{
    public static class EnumHelper
    {
        public static IEnumerable<T> GetAllValues<T>()
            where T: Enum
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
            
        }
    }
}

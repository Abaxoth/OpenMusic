﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OM.Core.Http
{
    public class UriQuery : ICollection<QueryParameter>, IEnumerable<QueryParameter>, IEnumerable
    {
        private readonly List<QueryParameter> _parameters;

        public UriQuery()
        {
            this._parameters = new List<QueryParameter>();
            this.SyncRoot = new object();
            this.IsSynchronized = false;
            this.IsReadOnly = false;
        }

        public int Count => _parameters.Count;
        public bool IsReadOnly { get; }
        public object SyncRoot { get; }
        public bool IsSynchronized { get; }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this._parameters.GetEnumerator();
        }

        public bool Contains(QueryParameter item)
        {
            return this._parameters.Contains(item);
        }

        public void CopyTo(QueryParameter[] array, int arrayIndex)
        {
            this._parameters.CopyTo(array, arrayIndex);
        }

        IEnumerator<QueryParameter> IEnumerable<QueryParameter>.GetEnumerator()
        {
            return (IEnumerator<QueryParameter>)this._parameters.GetEnumerator();
        }

        public bool Remove(QueryParameter item)
        {
            return this._parameters.Remove(item);
        }

        public void Add(string name, string key)
        {
            this.Add(new QueryParameter(name, key));
        }

        public void Add(QueryParameter parameter)
        {
            this._parameters.Add(parameter);
        }

        public void Add(KeyValuePair<string, string> parameter)
        {
            this.Add(parameter.Key, parameter.Value);
        }

        public string Get(string name)
        {
            foreach (var parameter in this._parameters)
            {
                if (parameter.Name == name)
                    return parameter.Value;
            }
            return (string)null;
        }

        public virtual string Get(int index)
        {
            return this._parameters[index].Value;
        }

        public List<string> GetValues(string name)
        {
            var stringList = (List<string>)null;
            foreach (var parameter in this._parameters)
            {
                if (parameter.Name == name)
                {
                    if (stringList == null)
                        stringList = new List<string>();
                    stringList.Add(parameter.Value);
                }
            }
            return stringList;
        }

        public void Remove(string name)
        {
            for (var index = 0; index < this._parameters.Count; ++index)
            {
                if (this._parameters[index].Name == name)
                {
                    this._parameters.RemoveAt(index);
                    --index;
                }
            }
        }

        public void Remove(int index)
        {
            this._parameters.RemoveAt(index);
        }

        private int GetIndexOfFirstAndRemoveOther(string name)
        {
            var num = -1;
            for (var index = 0; index < this._parameters.Count; ++index)
            {
                if (this._parameters[index].Name == name)
                {
                    if (num == -1)
                    {
                        num = index;
                    }
                    else
                    {
                        this._parameters.RemoveAt(index);
                        --index;
                    }
                }
            }
            return num;
        }

        public virtual void Set(string name, string value)
        {
            var firstAndRemoveOther = this.GetIndexOfFirstAndRemoveOther(name);
            if (firstAndRemoveOther == -1)
                this.Add(name, value);
            else
                this._parameters[firstAndRemoveOther].Value = value;
        }

        public string this[string name]
        {
            get => this.Get(name);
            set => this.Set(name, value);
        }

        public string this[int index] => Get(index);

        public void Clear()
        {
            this._parameters.Clear();
        }

        public override string ToString()
        {
            return AsQueryString(this._parameters, false, false);
        }

        public string ToString(bool encode)
        {
            return AsQueryString(this._parameters, false, encode);
        }

        public string ToString(bool addQuestionMark, bool encode)
        {
            return AsQueryString(this._parameters, addQuestionMark, encode);
        }

        private static string AsQueryString(
          IReadOnlyCollection<QueryParameter> parameters,
          bool addQuestionMark,
          bool encode)
        {
            if (parameters.Count == 0)
                return "";
            var stringBuilder = new StringBuilder(addQuestionMark ? "?" : "");
            var str1 = "&";
            foreach (QueryParameter queryParameter in parameters.Where<QueryParameter>((Func<QueryParameter, bool>)(kvp => kvp.Value != null)))
            {
                var str2 = encode ? WebUtility.UrlEncode(queryParameter.Name) : queryParameter.Name;
                var str3 = encode ? WebUtility.UrlEncode(queryParameter.Value) : queryParameter.Value;
                stringBuilder.AppendFormat("{1}={2}{0}", (object)str1, (object)str2, (object)str3);
            }
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            return stringBuilder.ToString();
        }

    }
}

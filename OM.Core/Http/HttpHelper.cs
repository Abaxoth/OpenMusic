﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace OM.Core.Http
{
    public static class HttpHelper
    {
        public static async Task<T> GetAsync<T>(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url));
            return await ProcessRequestAsync<T>(request);
        }

        public static async Task<T> PostAsync<T>(string url, object body)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(url))
            {
                Content = JsonContent.Create(body),
            };

            return await ProcessRequestAsync<T>(request);
        }

        private static async Task<T> ProcessRequestAsync<T>(HttpRequestMessage request)
        {
            using var client = new HttpClient();
            var response = await client.SendAsync(request);

            if(!response.IsSuccessStatusCode)
                throw new Exception(await response.Content.ReadAsStringAsync());

            var responseStr = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<T>(responseStr);
            return result;
        }

    }
}

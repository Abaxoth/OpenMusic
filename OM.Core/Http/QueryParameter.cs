﻿namespace OM.Core.Http
{
    public class QueryParameter
    {
        public QueryParameter()
        {
        }

        public QueryParameter(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        public string Name { set; get; }
        public string Value { set; get; }

        public override string ToString()
        {
            return this.Name + ": " + this.Value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if ((object)this == obj)
                return true;
            return !(obj.GetType() != this.GetType()) && this.Equals((QueryParameter)obj);
        }

        protected bool Equals(QueryParameter other)
        {
            return string.Equals(this.Name, other.Name) && string.Equals(this.Value, other.Value);
        }

        public override int GetHashCode()
        {
            return (this.Name != null ? this.Name.GetHashCode() : 0) * 397 ^ (this.Value != null ? this.Value.GetHashCode() : 0);
        }

        public static bool operator ==(QueryParameter left, QueryParameter right)
        {
            return object.Equals((object)left, (object)right);
        }

        public static bool operator !=(QueryParameter left, QueryParameter right)
        {
            return !object.Equals((object)left, (object)right);
        }
    }
}

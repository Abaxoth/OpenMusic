﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OM.Core.Extensions
{
    public static class AssemblyEx
    {
        public static IReadOnlyCollection<Type> FindTypesWithAttribute<TAttribute>(this Assembly assembly)
            where TAttribute: Attribute
        {
            var types = assembly.GetTypes().Where(x => x.GetCustomAttribute<TAttribute>() != null);
            return types.ToList();
        }
    }
}

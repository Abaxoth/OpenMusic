﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OM.Core.Extensions.Models;

namespace OM.Core.Extensions
{
    public static class SemaphoreEx
    {
        public static async Task<SemaphoreReleaseUnit> JoinAsync(this SemaphoreSlim semaphore)
        {
            await semaphore.WaitAsync();
            return new SemaphoreReleaseUnit(semaphore);
        }
    }
}

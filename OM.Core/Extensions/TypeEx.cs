﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OM.Core.Extensions
{
    public static class TypeEx
    {
        public static TObject NotNull<TObject>(this TObject obj, string exceptionMessage)
        {
            if (obj == null) throw new Exception(exceptionMessage);
            return obj;
        }

        public static TObject NotNull<TObject>(this TObject obj)
        {
            if (obj == null) throw new Exception($"Object can't be NULL");
            return obj;
        }

        public static TAttribute GetAttribute<TAttribute>(this Type type)
            where TAttribute: Attribute
        {
            var attribute = type.GetCustomAttribute<TAttribute>();
            if (attribute == null)
                throw new NullReferenceException($"Can't find attribute: {typeof(TAttribute)} in type: {type}");

            return attribute;
        }
    }
}

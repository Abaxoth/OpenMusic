﻿using System.Threading.Tasks;

namespace OM.Core.Extensions
{
    public static class TaskEx
    {
        public static async Task<TResult> OrDefaultAsync<TResult>(this Task<TResult> task)
        {
            try
            {
                return await task;
            }
            catch
            {
                return default;
            }
        }
    }
}

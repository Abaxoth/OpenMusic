﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OM.Core.Extensions.Models
{
    public class SemaphoreReleaseUnit : IDisposable
    {
        private readonly SemaphoreSlim _semaphore;

        public SemaphoreReleaseUnit(SemaphoreSlim semaphore)
        {
            _semaphore = semaphore;
        }

        public void Exit()
        {
            _semaphore?.Release();
        }

        public void Dispose()
        {
            Exit();
        }
    }
}
